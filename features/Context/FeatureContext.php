<?php

namespace Context;

use Behat\Behat\Tester\Exception\PendingException;
use Behat\Behat\Context\Context;
use Behat\Gherkin\Node\PyStringNode;
use Behat\Gherkin\Node\TableNode;
use Behat\MinkExtension\Context\MinkContext;
use Behat\Symfony2Extension\Context\KernelAwareContext;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends MinkContext implements Context,  KernelAwareContext
{
    private $kernel;

    public function setKernel(KernelInterface $kernel)
    {
        $this->kernel = $kernel;
    }
    protected function getContainer()
    {
        return $this->kernel->getContainer();
    }

    /**
     * @Given /^I wait for (\d+) seconds$/
     */
    public function iWaitForSeconds($seconde)
    {
        $this->getSession()->wait($seconde * 1000);
    }

    /**
     * @When /^I click li option "([^"]*)"$/
     *
     * @param $text
     * @throws \InvalidArgumentException
     */
    public function iClickLiOption($text)
    {
        $session = $this->getSession();
        $element = $session->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath('xpath', '*//*[text()="'. $text .'"]')
        );

        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Cannot find text: "%s"', $text));
        }

        $element->click();
    }

    /**
     * Click on the element with the provided CSS Selector
     *
     * @When /^I click on the element with css selector "([^"]*)"$/
     */
    public function iClickOnTheElementWithCSSSelector($cssSelector)
    {
        $session = $this->getSession();
        $element = $session->getPage()->find(
            'xpath',
            $session->getSelectorsHandler()->selectorToXpath('css', $cssSelector) // just changed xpath to css
        );
        if (null === $element) {
            throw new \InvalidArgumentException(sprintf('Could not evaluate CSS Selector: "%s"', $cssSelector));
        }

        $element->click();

    }

    /**
     * Initializes context.
     *
     * Every scenario gets its own context instance.
     * You can also pass arbitrary arguments to the
     * context constructor through behat.yml.
     */
    public function __construct()
    {
    }


    /**
     * @Given |I am on :url page
     */
    public function iAmOnPage($url)
    {
        $session = $this->getSession();
        $session->visit($this->locatePath($url));
        //throw new PendingException();
    }

//    /**
//     * @Then |I should see :arg1
//     */
//    public function iShouldSee($arg1)
//    {
//        throw new PendingException();
//    }
//
//    /**
//     * @When |I fill in :arg1 with :arg2
//     */
//    public function iFillInWith($arg1, $arg2)
//    {
//        $this->fillField('appbundle_offre['.$arg1.']', $arg2);
//        //throw new PendingException();
//    }
//
//    /**
//     * @When |I press :arg1
//     */
//    public function iPress($arg1)
//    {
//        $this->pressButton($arg1);
//       // throw new PendingException();
//    }
}
