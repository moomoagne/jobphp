-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  ven. 10 août 2018 à 17:32
-- Version du serveur :  5.7.21
-- Version de PHP :  7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `livindkr_api`
--

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

DROP TABLE IF EXISTS `article`;
CREATE TABLE IF NOT EXISTS `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `contenu` longtext COLLATE utf8_unicode_ci,
  `date_article` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nb_lecteur` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom_auteur` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `lien_blog` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_23A0E666B3CA4B` (`id_user`),
  KEY `IDX_23A0E665697F554` (`id_category`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
CREATE TABLE IF NOT EXISTS `categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `desription` longtext COLLATE utf8_unicode_ci,
  `is_entity` tinyint(1) DEFAULT NULL,
  `identifiant` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `categorie`
--

INSERT INTO `categorie` (`id`, `nom`, `image`, `desription`, `is_entity`, `identifiant`) VALUES
(1, 'Restaurants', 'restaurant.jpg', 'lorem lorem', 1, 'restaurants'),
(2, 'Bien-Être', NULL, NULL, 1, 'bien-etre'),
(3, 'Mode & Beauté', NULL, NULL, 1, 'mode-beaute'),
(4, 'Night Life', 'nightlife.jpg', NULL, 1, 'night-life'),
(5, 'Prestataires', 'Prestataires.jpg', NULL, 1, 'prestataires'),
(6, 'Shopping', NULL, NULL, 1, 'shopping'),
(7, 'Thing To Do', NULL, NULL, 1, 'thing-to-do'),
(8, 'Découverte', 'decouverte.jpg', NULL, 0, 'decouverte'),
(13, 'Evénement', 'evenement.jpg', NULL, NULL, 'evenement');

-- --------------------------------------------------------

--
-- Structure de la table `composant`
--

DROP TABLE IF EXISTS `composant`;
CREATE TABLE IF NOT EXISTS `composant` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu_id` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `prix` int(11) NOT NULL,
  `vignette` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_EC8486C9CCD7E912` (`menu_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `composant`
--

INSERT INTO `composant` (`id`, `menu_id`, `titre`, `prix`, `vignette`, `details`) VALUES
(1, 1, 'chocolat', 2000, NULL, '');

-- --------------------------------------------------------

--
-- Structure de la table `event`
--

DROP TABLE IF EXISTS `event`;
CREATE TABLE IF NOT EXISTS `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_institution` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `date_event` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `lieu` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `siteweb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3BAE0AA7228D5512` (`id_institution`),
  KEY `IDX_3BAE0AA76B3CA4B` (`id_user`),
  KEY `IDX_3BAE0AA75697F554` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `event`
--

INSERT INTO `event` (`id`, `id_institution`, `id_user`, `id_category`, `date_event`, `description`, `nom`, `photo`, `status`, `lieu`, `telephone`, `email`, `siteweb`, `facebook`, `twitter`, `instagram`) VALUES
(1, NULL, 1, 2, '08/02/2018 - 08/02/2018', '<p><b>Bla </b>bla bla !!!!</p>', 'Séminaire sur la gestion de l\'eau et de l\'environnement', NULL, 0, '', '', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `event_photo`
--

DROP TABLE IF EXISTS `event_photo`;
CREATE TABLE IF NOT EXISTS `event_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_event` int(11) DEFAULT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `date_creation` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_55AC3534D52B4B97` (`id_event`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `event_photo`
--

INSERT INTO `event_photo` (`id`, `id_event`, `url`, `nom`, `date_creation`) VALUES
(1, 1, 'http://localhost/livindkr_api/web/images/user2-160x160.jpg', 'monteur en scène', '2018-08-08 17:31:00');

-- --------------------------------------------------------

--
-- Structure de la table `event_tag`
--

DROP TABLE IF EXISTS `event_tag`;
CREATE TABLE IF NOT EXISTS `event_tag` (
  `event_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  PRIMARY KEY (`event_id`,`tag_id`),
  KEY `IDX_1246725071F7E88B` (`event_id`),
  KEY `IDX_12467250BAD26311` (`tag_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `fos_user`
--

DROP TABLE IF EXISTS `fos_user`;
CREATE TABLE IF NOT EXISTS `fos_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `username_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `email_canonical` varchar(180) COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `salt` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `confirmation_token` varchar(180) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password_requested_at` datetime DEFAULT NULL,
  `roles` longtext COLLATE utf8_unicode_ci NOT NULL COMMENT '(DC2Type:array)',
  `date_naissance` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_dakar` tinyint(1) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `prenom` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `dateinscription` datetime DEFAULT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `sexe` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `code_generate` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UNIQ_957A647992FC23A8` (`username_canonical`),
  UNIQUE KEY `UNIQ_957A6479A0D96FBF` (`email_canonical`),
  UNIQUE KEY `UNIQ_957A6479C05FB297` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `fos_user`
--

INSERT INTO `fos_user` (`id`, `username`, `username_canonical`, `email`, `email_canonical`, `enabled`, `salt`, `password`, `last_login`, `confirmation_token`, `password_requested_at`, `roles`, `date_naissance`, `is_dakar`, `nom`, `prenom`, `photo`, `telephone`, `dateinscription`, `adresse`, `sexe`, `code_generate`) VALUES
(1, 'super_admin', 'super_admin', 'super_admin@livindkr.com', 'super_admin@livindkr.com', 1, NULL, '$2y$13$LyDMKp.GA/CotwxMBedMn.gBp9Gkt2bpVIqQdxVBG1xYh2bx7EZ5a', '2018-08-09 15:08:01', NULL, NULL, 'a:1:{i:0;s:16:\"ROLE_SUPER_ADMIN\";}', '10-08-1989', 1, 'Sény', 'Mbaye', NULL, '775915416', NULL, 'Hlm grand yoff', NULL, NULL),
(2, 'admin', 'admin', 'admin@livindkr.com', 'admin@livindkr.com', 1, NULL, '$2y$13$CiD/SaWboqswbEzg6KYYA.ufIv9Yj6HlibXrbvsEpxOyCW//9hX4e', '2018-07-27 19:03:28', NULL, NULL, 'a:1:{i:0;s:10:\"ROLE_ADMIN\";}', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 'senyr9', 'senyr9', 'senyr9@hotmail.com', 'senyr9@hotmail.com', 0, NULL, '$2y$13$dW5pL6gCrYEsxzEcKSIgyeiZT6VLdQZ4uYias4MS1WYIcXlB6aGL.', NULL, 'wZE1PJ6sWIiceYR5nyCMJcxzKfzBQPO-T3cjhQVeDkQ', NULL, 'a:0:{}', NULL, NULL, NULL, NULL, NULL, NULL, '2018-07-26 15:45:11', NULL, NULL, NULL),
(9, 'senol', 'senol', 'seny.mbaye@gmail.com', 'seny.mbaye@gmail.com', 1, NULL, '$2y$13$OmnF7dIh..Kl8B/Fr/kg0OZY3qwYM6uhy71g/DZymy3H12OnCqZFq', '2018-08-09 18:37:10', NULL, NULL, 'a:0:{}', NULL, NULL, 'Mbaye', 'Sény', NULL, NULL, '2018-08-09 16:56:22', NULL, 'Homme', '5963');

-- --------------------------------------------------------

--
-- Structure de la table `galerie`
--

DROP TABLE IF EXISTS `galerie`;
CREATE TABLE IF NOT EXISTS `galerie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `galerie`
--

INSERT INTO `galerie` (`id`, `nom`, `path`) VALUES
(1, 'Capture.PNG', 'C:\\wamp\\tmp\\phpE48F.tmp'),
(2, 'ad7d37-0c7ce21326094814961b385f7c990eac-mv2.jpg', 'C:\\wamp\\tmp\\php227C.tmp');

-- --------------------------------------------------------

--
-- Structure de la table `institution`
--

DROP TABLE IF EXISTS `institution`;
CREATE TABLE IF NOT EXISTS `institution` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telephone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  `latitude` double DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `promo` tinyint(1) DEFAULT NULL,
  `solde` tinyint(1) DEFAULT NULL,
  `offre_id` int(11) DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `siteweb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_sous_category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_3A9F98E54CC8505A` (`offre_id`),
  KEY `IDX_3A9F98E55697F554` (`id_category`),
  KEY `IDX_3A9F98E56B3CA4B` (`id_user`),
  KEY `IDX_3A9F98E5B02AFCF1` (`id_sous_category`)
) ENGINE=InnoDB AUTO_INCREMENT=276 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `institution`
--

INSERT INTO `institution` (`id`, `nom`, `description`, `adresse`, `telephone`, `price`, `longitude`, `latitude`, `photo`, `type`, `promo`, `solde`, `offre_id`, `id_category`, `email`, `facebook`, `twitter`, `instagram`, `id_user`, `siteweb`, `status`, `id_sous_category`) VALUES
(1, 'Ocheese', 'Fromage à gogo', 'Sicap Foire', '0772232838', '1200', NULL, NULL, NULL, 'restaurants', 1, NULL, NULL, 1, 'contact@ocheese.com', 'http://facebook.com', 'http://twitter.com', 'http://instagram.com', 1, NULL, NULL, 1),
(2, 'Pharmacie Hafia', 'Ventes de produits bio', 'Hlm grand yoff', '0772232838', '1200', NULL, NULL, '17fd8b133c5ba9354e15bd8df22ce48d.png', 'bien_etre', NULL, NULL, NULL, 2, 'contact@hafia.com', 'http://facebook.com', 'http://twitter.com', 'http://instagram.com', 1, NULL, NULL, NULL),
(3, 'Al-mahdi', 'fast-food', 'Yoff', '77656454', '3000', 12345775, 12348743, '6ee35c18e9251c558ea4459d3c6f8cf3.jpeg', 'restaurants', 0, NULL, 2, 1, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL),
(74, 'Le Gastronomique du Terrou-Bi ', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(75, 'La Terrasse du Terrou-Bi ', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(76, 'Le Grain de Sel du Terrou-Bi', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(77, 'Cîte Ouest Food and Bar', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(78, 'Pullman', NULL, '10, Rue Colbert, Place de L\' independance, Dakar 18524', '33 889 22 00', 'http://www.pullmanhotels.com/gb/hotel-0563-pullman-dakar-teranga/index.shtml', -17.431448, 14.667862, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dakar.reservation@accor.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(79, 'Chez Katia', NULL, 'Route de l\'Aeroport, Dakar', '33 820 80 82', '', -17.507147, 14.748476, NULL, 'restaurants', NULL, NULL, NULL, 1, 'katia2009@orange.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(80, 'Alkimia', NULL, 'Route du King Fahd Palace - Almadies', '33 820 68 68', 'http://www.alkimiadakar.com/bar_restau.html', -17.51563, 14.739949, NULL, 'restaurants', NULL, NULL, NULL, 1, 'alkimia@fourchettedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(81, 'Parrilla', NULL, 'Rue Emile Zola, Dakar', '33 822 15 00', 'http://groupelaparrilla.com/laparrilla/index.php', -17.434433, 14.664184, NULL, 'restaurants', NULL, NULL, NULL, 1, 'laparrilla@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(82, 'The HUB', NULL, '93, Boulevard de Ziguinchor, Dakar', '76 247 99 35', 'https://www.facebook.com/thehubdakar/', -17.456077, 14.693687, NULL, 'restaurants', NULL, NULL, NULL, 1, 'thehubdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(83, 'Le Lagon', NULL, 'Route de la Petite Corniche Est ', '33 823 67 69', 'http://www.lelagondakar.com/lelrestaurant.html', -17.428442, 14.667758, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@lelagondakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(84, 'Mokai', NULL, '69 rue Carnot- Plateau', '33 823 16 23', '', -17.435251, 14.667663, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lemokai@ymail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(85, 'Le Jardin', NULL, 'Pointe des Almadies', '33 820 77 03', 'http://lapointedesalmadies.sn/', -17.527958, 14.745585, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lapointe@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(86, 'La Fourchette', NULL, '6 rue Parchappe-Plateau', '33 821 88 87', 'http://www.fourchettedakar.com/bar_restau.html', -17.427975, 14.670455, NULL, 'restaurants', NULL, NULL, NULL, 1, 'fourchette@fourchettedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(87, 'L\'avenue', NULL, 'Avenue de la Corniche Ouest | Radisson Blu Hotel', '33 869 33 33', 'https://www.radissonblu.com/en/hotel-dakar/restaurants#/lavenue-restaurant', -17.47339, 14.697042, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info.dakar@radissonblu.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(88, 'Marina Bay', NULL, 'Route De L\'Aeroport | N\'gor Almadies, Rond point du Casino du Cap-Vert', ' 33 820 03 30', 'http://www.marinabay.sn/', -17.502343, 14.752506, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@marinabay.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(89, 'Le Galuchat', NULL, 'Radisson Blu, Hotel Dakar Sea Plaza', '33 869 33 53', 'https://www.radissonblu.com/fr/hotel-dakar/restaurants', -17.524023, 14.747459, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info.dakar@radissonblu.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(90, 'Les Jardins de l\'Ocean ', NULL, 'Mridien Prsident Almadies Route des Almadies, Dakar', '33 869 69 69', 'http://www.kingfahdpalacehotels.com/v2/en/restaurants-bars/les-jardins-de-locean/', -17.524057, 14.746969, NULL, 'restaurants', NULL, NULL, NULL, 1, 'informations.dakar@kingfahdpalacehotels.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(91, 'La Pointe des Almadies ', NULL, 'Pointe des Almadies-Almadies', '33 820 01 40', 'http://lapointedesalmadies.sn/', -17.528049, 14.745513, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lapointe@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(92, 'Noflaye Beach', NULL, 'Corniche des Almadies, Ngor, Senegal', '33 820 30 32/77 6345718', '', -17.521879, 14.741283, NULL, 'restaurants', NULL, NULL, NULL, 1, 'oumtra@hotmail.fr', NULL, NULL, NULL, 1, '', NULL, NULL),
(93, 'La Piazzola', NULL, 'Villa 86 Sotrac-Mermoz 1 rue apres le dpot DDD', '33 860 36 14', 'https://lapiazzolasn.com/', -17.479745, 14.70855, NULL, 'restaurants', NULL, NULL, NULL, 1, 'jidablade@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(94, 'Red Spoon ', NULL, 'Au Red Game, Sea Plaza Corniche Ouest', '33 859 91 90', 'http://redgames.sn/category/red-games/redspoon/', -17.474716, 14.693149, NULL, 'restaurants', NULL, NULL, NULL, 1, 'redgames@redgames.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(95, 'Sharky\'s', NULL, 'Corniche almadies, Rue ng-172, Dakar', '77 326 21 56', 'http://sharkys.sn/', -17.520351, 14.740674, NULL, 'restaurants', NULL, NULL, NULL, 1, 'hello@sharkys.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(96, 'O sushi Bar', NULL, 'Route de la Corniche O, Dakar', '33 820 54 44', 'http://www.osushibar.com/', -17.51012, 14.739038, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(97, 'SAVEURS D\'ASIE BOURGUIBA', NULL, 'Boulevard du President Habib Bourguiba', '33 824 56 62', 'http://www.saveursdasie-sn.com/', -17.46169, 14.706062, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(98, 'SAVEURS D\'ASIE SEA PLAZA', NULL, 'Fann', '33 824 88 08', 'http://www.saveursdasie-sn.com/', -17.474437, 14.694071, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(99, 'SAVEURS D\'ASIE NGOR', NULL, 'Route de l?Aroport', '33 820 66 05', 'http://www.saveursdasie-sn.com/', -17.509474, 14.746957, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(100, 'SAVEURS D\'ASIE PLATEAU', NULL, 'Rue de Thann', '33 821 47 74', 'http://www.saveursdasie-sn.com/', -17.4314389, 14.6713156, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(101, 'Sharky\'s', NULL, 'Corniche almadies, Rue ng-172, Dakar', '77 326 21 56', 'http://sharkys.sn/', -17.520383, 14.740902, NULL, 'restaurants', NULL, NULL, NULL, 1, 'hello@sharkys.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(102, 'Le Colorado', NULL, 'Boulevard de l\'est xRue de Kaolack,Point E', '76 644 64 04', 'https://www.lecolorado.com/', -17.457986, 14.695148, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@lecolorado.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(103, 'Ciao Italia', NULL, '21, Rue Huart, Dakar', '33 842 32 22', '', -17.430044, 14.671404, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(104, 'La Piazzola', NULL, 'Mermoz route de Ouakam | prêt des bus DDD', '33 860 36 14', 'https://lapiazzolasn.com/', -17.479743, 14.708768, NULL, 'restaurants', NULL, NULL, NULL, 1, 'jidablade@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(105, 'La Calebasse', NULL, 'Mamelles, Route des Almadies', '33 860 69 47', '', -17.502432, 14.72756, NULL, 'restaurants', NULL, NULL, NULL, 1, 'restaurantlacalebasse@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(106, 'Heritage', NULL, 'Boulevard du President Habib Bourguiba', '33 824 05 06', '', -17.464841, 14.701864, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(107, 'Just 4 U', NULL, 'Avenue Cheikh Anta Diop, Dakar', '33 825 99 25', '', -17.460301, 14.692071, NULL, 'restaurants', NULL, NULL, NULL, 1, 'justdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(108, 'Le Thiof', NULL, 'Sicap Baobab, Rue 9 bis', '33 824 62 07', '', -17.465271, 14.70725, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lethiof@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(109, 'Le Xalam', NULL, 'Virage, Yoff, Dakar', '77 605 32 98', 'https://www.facebook.com/Le-Xalam-772947462773814/', -17.491733, 14.755826, NULL, 'restaurants', NULL, NULL, NULL, 1, 'papispa.sylla5@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(110, 'Yuma', NULL, 'Amiti 2, Alles Seydou Nourou Tall de Citydia', '33 859 25 05', 'http://www.yumarestaurant.com/', -17.461982, 14.7036, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@yumarestaurant.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(111, 'Annapurna India', NULL, 'Route de Ngor, en face du stade', '77 394 15 90', 'http://www.annapurnadakar.com', -17.509788, 14.746099, NULL, 'restaurants', NULL, NULL, NULL, 1, 'sandeepmanmati@yahoo.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(112, 'Le Maharajah', NULL, 'Route de l\'Aeroport entre Ngor et Virage', '76 606 17 27', '', -17.494046, 14.75443, NULL, 'restaurants', NULL, NULL, NULL, 1, 'maharajadakar123@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(113, 'Chick\'N Corner', NULL, '36, Rue Victor Hugo, Dakar 23325', '33 829 28 28', 'http://the-chickn-corner.business.site/', -17.435703, 14.666135, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@thechickncorner.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(114, 'La Galette ', NULL, '16, Av Pompidou, centre ville', '33 823 63 63', '', -17.433413, 14.669935, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lagalette@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(115, 'LGM- Le Glacier Moderne', NULL, 'Boulevard de l\'Est x rue 4 / POINT E', '33 825 54 54', '', -17.457102, 14.69681, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(116, 'Eric Kayser', NULL, '3, Boulevard de la Rpublique', '33 822 81 80', 'http://www.maison-kayser.com/fr/', -17.434322, 14.66495, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@maison-kayser.com\nerickayserdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(117, 'Feel Juice', NULL, '88, rue Flix Faure, Dakar', '33 821 54 00', '', -17.438351, 14.667291, NULL, 'restaurants', NULL, NULL, NULL, 1, 'feeljuice@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(118, 'CI GUSTA', NULL, '143, Avenue Lamine Gueye, Dakar BP 4399', '33 821 96 37', 'https://www.cigusta.com/stores/372/', -17.436926, 14.666341, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@cigusta.com\nlouna.hoballah@cigusta.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(119, 'Patisserie Preira', NULL, 'SC-152 iba nbaye diadji', '33 824 07 53', '', -17.465664, 14.71399, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(120, 'Yougurtlandia', NULL, 'Rue MZ-208, Dakar', '33 860 88 89', 'http://www.yogurtlandia.com/', -17.480089, 14.708368, NULL, 'restaurants', NULL, NULL, NULL, 1, 'yogurtlandiadkr@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(121, 'N\'ice Cream', NULL, '97 Avenue Andr Peytavin. Immeuble Kb, Dakar', '33 823 35 45', 'http://www.nicecream.sn/', -17.440304, 14.669055, NULL, 'restaurants', NULL, NULL, NULL, 1, 'nicecream@arc.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(135, 'Sharky\'s', NULL, 'Corniche almadies, Rue ng-172, Dakar', '77 326 21 56', 'http://sharkys.sn/', -17.520351, 14.740674, NULL, 'restaurants', NULL, NULL, NULL, 1, 'hello@sharkys.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(136, 'Teranga Lounge by Pullman', NULL, '10 rue Colbert, Place de l\'Independance au Pullman', '78 631 82 82', 'https://www.accorhotels.com/gb/hotel-0563-pullman-dakar-teranga/index.shtml', -17.430782, 14.667953, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dakar.reservation@accor.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(137, 'Time\'s Caf', NULL, '27 avenue Leopold Sedar Senghor', '33 821 21 68', '', -17.432716, 14.666531, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(138, 'Z\'grill', NULL, 'Point E, Boulevard de l\'Est ( ct du Lgm)', '33 825 50 52', '', -17.456854, 14.696819, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(139, 'Bidew', NULL, '89 Rue Joseph Gomis x Carnot, Dakar, Senegal', '33 823 03 20', '', -17.435501, 14.668143, NULL, 'restaurants', NULL, NULL, NULL, 1, 'restaurantlebideew@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(140, 'Bayekou', NULL, 'Parking embarcadre de Ngor-Ngor', '77 681 38 88', '', -17.511015, 14.749825, NULL, 'restaurants', NULL, NULL, NULL, 1, 'bayekoudk@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(141, 'Churrascaria Brasil', NULL, '7 Rue Filla Fann-Hock, Dakar, Senegal', '33 821 00 32', '', -17.463047, 14.679685, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(142, 'Redstone', NULL, '21, route des Almadies, Dakar 12000', '33 820 20 55', 'https://www.redstonedakar.com/', -17.521332, 14.743925, NULL, 'restaurants', NULL, NULL, NULL, 1, 'redstone@redstonedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(143, 'Sao Brasil', NULL, 'Route de Ngor, Dakar, Senegal', '33 820 09 41', '', -17.507792, 14.748994, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(152, 'Simone Caf', NULL, '#7603, Villa Nabou, Rue 3, Dakar', '33 825 11 98', 'http://simonecafe.com/fr/restaurant/', -17.473489, 14.706277, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@simonecafe.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(153, 'Esprit Sushi', NULL, 'Immeuble Goeland, Boulevard El Hadj Djily Mbaye, Dakar', '33 821 70 00', '', -17.432855, 14.673344, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(154, 'Le Jardin Thailandais', NULL, 'Boulevard de Ziguinchor, Dakar', '33 825 58 33', 'https://le-jardin-thailandais.business.site/', -17.459905, 14.694516, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lejardinthailandais@yahoo.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(155, 'Little Buddha', NULL, 'Sea Plaza,, Dakar', '33 859 93 80', '', -17.474541, 14.693911, NULL, 'restaurants', NULL, NULL, NULL, 1, 'jean-luc.gbaguidi@radissonblu.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(156, 'Le Gastronomique du Terrou-Bi ', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(157, 'La Terrasse du Terrou-Bi ', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(158, 'Le Grain de Sel du Terrou-Bi', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(159, 'Cîte Ouest Food and Bar', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(160, 'Pullman', NULL, '10, Rue Colbert, Place de L\' independance, Dakar 18524', '33 889 22 00', 'http://www.pullmanhotels.com/gb/hotel-0563-pullman-dakar-teranga/index.shtml', -17.431448, 14.667862, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dakar.reservation@accor.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(161, 'Chez Katia', NULL, 'Route de l\'Aeroport, Dakar', '33 820 80 82', '', -17.507147, 14.748476, NULL, 'restaurants', NULL, NULL, NULL, 1, 'katia2009@orange.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(162, 'Alkimia', NULL, 'Route du King Fahd Palace - Almadies', '33 820 68 68', 'http://www.alkimiadakar.com/bar_restau.html', -17.51563, 14.739949, NULL, 'restaurants', NULL, NULL, NULL, 1, 'alkimia@fourchettedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(163, 'Parrilla', NULL, 'Rue Emile Zola, Dakar', '33 822 15 00', 'http://groupelaparrilla.com/laparrilla/index.php', -17.434433, 14.664184, NULL, 'restaurants', NULL, NULL, NULL, 1, 'laparrilla@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(164, 'The HUB', NULL, '93, Boulevard de Ziguinchor, Dakar', '76 247 99 35', 'https://www.facebook.com/thehubdakar/', -17.456077, 14.693687, NULL, 'restaurants', NULL, NULL, NULL, 1, 'thehubdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(165, 'Le Lagon', NULL, 'Route de la Petite Corniche Est ', '33 823 67 69', 'http://www.lelagondakar.com/lelrestaurant.html', -17.428442, 14.667758, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@lelagondakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(166, 'Mokai', NULL, '69 rue Carnot- Plateau', '33 823 16 23', '', -17.435251, 14.667663, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lemokai@ymail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(167, 'Le Jardin', NULL, 'Pointe des Almadies', '33 820 77 03', 'http://lapointedesalmadies.sn/', -17.527958, 14.745585, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lapointe@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(168, 'La Fourchette', NULL, '6 rue Parchappe-Plateau', '33 821 88 87', 'http://www.fourchettedakar.com/bar_restau.html', -17.427975, 14.670455, NULL, 'restaurants', NULL, NULL, NULL, 1, 'fourchette@fourchettedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(169, 'L\'avenue', NULL, 'Avenue de la Corniche Ouest | Radisson Blu Hotel', '33 869 33 33', 'https://www.radissonblu.com/en/hotel-dakar/restaurants#/lavenue-restaurant', -17.47339, 14.697042, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info.dakar@radissonblu.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(170, 'Marina Bay', NULL, 'Route De L\'Aeroport | N\'gor Almadies, Rond point du Casino du Cap-Vert', ' 33 820 03 30', 'http://www.marinabay.sn/', -17.502343, 14.752506, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@marinabay.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(171, 'Le Galuchat', NULL, 'Radisson Blu, Hotel Dakar Sea Plaza', '33 869 33 53', 'https://www.radissonblu.com/fr/hotel-dakar/restaurants', -17.524023, 14.747459, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info.dakar@radissonblu.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(172, 'Les Jardins de l\'Ocean ', NULL, 'Mridien Prsident Almadies Route des Almadies, Dakar', '33 869 69 69', 'http://www.kingfahdpalacehotels.com/v2/en/restaurants-bars/les-jardins-de-locean/', -17.524057, 14.746969, NULL, 'restaurants', NULL, NULL, NULL, 1, 'informations.dakar@kingfahdpalacehotels.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(173, 'La Pointe des Almadies ', NULL, 'Pointe des Almadies-Almadies', '33 820 01 40', 'http://lapointedesalmadies.sn/', -17.528049, 14.745513, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lapointe@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(174, 'Noflaye Beach', NULL, 'Corniche des Almadies, Ngor, Senegal', '33 820 30 32/77 6345718', '', -17.521879, 14.741283, NULL, 'restaurants', NULL, NULL, NULL, 1, 'oumtra@hotmail.fr', NULL, NULL, NULL, 1, '', NULL, NULL),
(175, 'La Piazzola', NULL, 'Villa 86 Sotrac-Mermoz 1 rue apres le dpot DDD', '33 860 36 14', 'https://lapiazzolasn.com/', -17.479745, 14.70855, NULL, 'restaurants', NULL, NULL, NULL, 1, 'jidablade@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(176, 'Red Spoon ', NULL, 'Au Red Game, Sea Plaza Corniche Ouest', '33 859 91 90', 'http://redgames.sn/category/red-games/redspoon/', -17.474716, 14.693149, NULL, 'restaurants', NULL, NULL, NULL, 1, 'redgames@redgames.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(177, 'Sharky\'s', NULL, 'Corniche almadies, Rue ng-172, Dakar', '77 326 21 56', 'http://sharkys.sn/', -17.520351, 14.740674, NULL, 'restaurants', NULL, NULL, NULL, 1, 'hello@sharkys.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(178, 'Teranga Lounge by Pullman', NULL, '10 rue Colbert, Place de l\'Independance au Pullman', '78 631 82 82', 'https://www.accorhotels.com/gb/hotel-0563-pullman-dakar-teranga/index.shtml', -17.430782, 14.667953, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dakar.reservation@accor.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(179, 'Time\'s Caf', NULL, '27 avenue Leopold Sedar Senghor', '33 821 21 68', '', -17.432716, 14.666531, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(180, 'Z\'grill', NULL, 'Point E, Boulevard de l\'Est ( ct du Lgm)', '33 825 50 52', '', -17.456854, 14.696819, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(181, 'Bidew', NULL, '89 Rue Joseph Gomis x Carnot, Dakar, Senegal', '33 823 03 20', '', -17.435501, 14.668143, NULL, 'restaurants', NULL, NULL, NULL, 1, 'restaurantlebideew@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(182, 'Bayekou', NULL, 'Parking embarcadre de Ngor-Ngor', '77 681 38 88', '', -17.511015, 14.749825, NULL, 'restaurants', NULL, NULL, NULL, 1, 'bayekoudk@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(183, 'Churrascaria Brasil', NULL, '7 Rue Filla Fann-Hock, Dakar, Senegal', '33 821 00 32', '', -17.463047, 14.679685, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(184, 'Redstone', NULL, '21, route des Almadies, Dakar 12000', '33 820 20 55', 'https://www.redstonedakar.com/', -17.521332, 14.743925, NULL, 'restaurants', NULL, NULL, NULL, 1, 'redstone@redstonedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(185, 'Sao Brasil', NULL, 'Route de Ngor, Dakar, Senegal', '33 820 09 41', '', -17.507792, 14.748994, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(186, 'Simone Caf', NULL, '#7603, Villa Nabou, Rue 3, Dakar', '33 825 11 98', 'http://simonecafe.com/fr/restaurant/', -17.473489, 14.706277, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@simonecafe.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(187, 'Esprit Sushi', NULL, 'Immeuble Goeland, Boulevard El Hadj Djily Mbaye, Dakar', '33 821 70 00', '', -17.432855, 14.673344, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(188, 'Le Jardin Thailandais', NULL, 'Boulevard de Ziguinchor, Dakar', '33 825 58 33', 'https://le-jardin-thailandais.business.site/', -17.459905, 14.694516, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lejardinthailandais@yahoo.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(189, 'O sushi Bar', NULL, 'Route de la Corniche O, Dakar', '33 820 54 44', 'http://www.osushibar.com/', -17.51012, 14.739038, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(190, 'SAVEURS D\'ASIE BOURGUIBA', NULL, 'Boulevard du President Habib Bourguiba', '33 824 56 62', 'http://www.saveursdasie-sn.com/', -17.46169, 14.706062, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(191, 'SAVEURS D\'ASIE SEA PLAZA', NULL, 'Fann', '33 824 88 08', 'http://www.saveursdasie-sn.com/', -17.474437, 14.694071, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(192, 'SAVEURS D\'ASIE NGOR', NULL, 'Route de l?Aroport', '33 820 66 05', 'http://www.saveursdasie-sn.com/', -17.509474, 14.746957, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(193, 'SAVEURS D\'ASIE PLATEAU', NULL, 'Rue de Thann', '33 821 47 74', 'http://www.saveursdasie-sn.com/', -17.4314389, 14.6713156, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(194, 'Sharky\'s', NULL, 'Corniche almadies, Rue ng-172, Dakar', '77 326 21 56', 'http://sharkys.sn/', -17.520383, 14.740902, NULL, 'restaurants', NULL, NULL, NULL, 1, 'hello@sharkys.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(195, 'Le Colorado', NULL, 'Boulevard de l\'est xRue de Kaolack,Point E', '76 644 64 04', 'https://www.lecolorado.com/', -17.457986, 14.695148, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@lecolorado.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(196, 'Ciao Italia', NULL, '21, Rue Huart, Dakar', '33 842 32 22', '', -17.430044, 14.671404, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(197, 'La Piazzola', NULL, 'Mermoz route de Ouakam | prêt des bus DDD', '33 860 36 14', 'https://lapiazzolasn.com/', -17.479743, 14.708768, NULL, 'restaurants', NULL, NULL, NULL, 1, 'jidablade@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(198, 'La Calebasse', NULL, 'Mamelles, Route des Almadies', '33 860 69 47', '', -17.502432, 14.72756, NULL, 'restaurants', NULL, NULL, NULL, 1, 'restaurantlacalebasse@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(199, 'Heritage', NULL, 'Boulevard du President Habib Bourguiba', '33 824 05 06', '', -17.464841, 14.701864, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(200, 'Just 4 U', NULL, 'Avenue Cheikh Anta Diop, Dakar', '33 825 99 25', '', -17.460301, 14.692071, NULL, 'restaurants', NULL, NULL, NULL, 1, 'justdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(201, 'Le Thiof', NULL, 'Sicap Baobab, Rue 9 bis', '33 824 62 07', '', -17.465271, 14.70725, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lethiof@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(202, 'Le Xalam', NULL, 'Virage, Yoff, Dakar', '77 605 32 98', 'https://www.facebook.com/Le-Xalam-772947462773814/', -17.491733, 14.755826, NULL, 'restaurants', NULL, NULL, NULL, 1, 'papispa.sylla5@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(203, 'Yuma', NULL, 'Amiti 2, Alles Seydou Nourou Tall de Citydia', '33 859 25 05', 'http://www.yumarestaurant.com/', -17.461982, 14.7036, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@yumarestaurant.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(204, 'Annapurna India', NULL, 'Route de Ngor, en face du stade', '77 394 15 90', 'http://www.annapurnadakar.com', -17.509788, 14.746099, NULL, 'restaurants', NULL, NULL, NULL, 1, 'sandeepmanmati@yahoo.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(205, 'Le Maharajah', NULL, 'Route de l\'Aeroport entre Ngor et Virage', '76 606 17 27', '', -17.494046, 14.75443, NULL, 'restaurants', NULL, NULL, NULL, 1, 'maharajadakar123@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(206, 'Chick\'N Corner', NULL, '36, Rue Victor Hugo, Dakar 23325', '33 829 28 28', 'http://the-chickn-corner.business.site/', -17.435703, 14.666135, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@thechickncorner.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(207, 'La Galette ', NULL, '16, Av Pompidou, centre ville', '33 823 63 63', '', -17.433413, 14.669935, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lagalette@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(208, 'LGM- Le Glacier Moderne', NULL, 'Boulevard de l\'Est x rue 4 / POINT E', '33 825 54 54', '', -17.457102, 14.69681, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(209, 'Eric Kayser', NULL, '3, Boulevard de la Rpublique', '33 822 81 80', 'http://www.maison-kayser.com/fr/', -17.434322, 14.66495, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@maison-kayser.com\nerickayserdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(210, 'Feel Juice', NULL, '88, rue Flix Faure, Dakar', '33 821 54 00', '', -17.438351, 14.667291, NULL, 'restaurants', NULL, NULL, NULL, 1, 'feeljuice@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(211, 'CI GUSTA', NULL, '143, Avenue Lamine Gueye, Dakar BP 4399', '33 821 96 37', 'https://www.cigusta.com/stores/372/', -17.436926, 14.666341, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@cigusta.com\nlouna.hoballah@cigusta.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(212, 'Patisserie Preira', NULL, 'SC-152 iba nbaye diadji', '33 824 07 53', '', -17.465664, 14.71399, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(213, 'Yougurtlandia', NULL, 'Rue MZ-208, Dakar', '33 860 88 89', 'http://www.yogurtlandia.com/', -17.480089, 14.708368, NULL, 'restaurants', NULL, NULL, NULL, 1, 'yogurtlandiadkr@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(214, 'N\'ice Cream', NULL, '97 Avenue Andr Peytavin. Immeuble Kb, Dakar', '33 823 35 45', 'http://www.nicecream.sn/', -17.440304, 14.669055, NULL, 'restaurants', NULL, NULL, NULL, 1, 'nicecream@arc.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(215, '', NULL, '', '', '', NULL, 0, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(216, 'Le Gastronomique du Terrou-Bi ', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(217, 'La Terrasse du Terrou-Bi ', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(218, 'Le Grain de Sel du Terrou-Bi', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(219, 'Cîte Ouest Food and Bar', NULL, 'Boulevard Martin Luther King| Hôtel Terrou Bi, Dakar', '33 839 90 39', 'http://www.terroubi.com/', -17.46604, 14.676864, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dirmarketing@terroubi.com\nreservation@terroubi.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(220, 'Pullman', NULL, '10, Rue Colbert, Place de L\' independance, Dakar 18524', '33 889 22 00', 'http://www.pullmanhotels.com/gb/hotel-0563-pullman-dakar-teranga/index.shtml', -17.431448, 14.667862, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dakar.reservation@accor.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(221, 'Chez Katia', NULL, 'Route de l\'Aeroport, Dakar', '33 820 80 82', '', -17.507147, 14.748476, NULL, 'restaurants', NULL, NULL, NULL, 1, 'katia2009@orange.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(222, 'Alkimia', NULL, 'Route du King Fahd Palace - Almadies', '33 820 68 68', 'http://www.alkimiadakar.com/bar_restau.html', -17.51563, 14.739949, NULL, 'restaurants', NULL, NULL, NULL, 1, 'alkimia@fourchettedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(223, 'Parrilla', NULL, 'Rue Emile Zola, Dakar', '33 822 15 00', 'http://groupelaparrilla.com/laparrilla/index.php', -17.434433, 14.664184, NULL, 'restaurants', NULL, NULL, NULL, 1, 'laparrilla@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(224, 'The HUB', NULL, '93, Boulevard de Ziguinchor, Dakar', '76 247 99 35', 'https://www.facebook.com/thehubdakar/', -17.456077, 14.693687, NULL, 'restaurants', NULL, NULL, NULL, 1, 'thehubdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(225, 'Le Lagon', NULL, 'Route de la Petite Corniche Est ', '33 823 67 69', 'http://www.lelagondakar.com/lelrestaurant.html', -17.428442, 14.667758, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@lelagondakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(226, 'Mokai', NULL, '69 rue Carnot- Plateau', '33 823 16 23', '', -17.435251, 14.667663, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lemokai@ymail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(227, 'Le Jardin', NULL, 'Pointe des Almadies', '33 820 77 03', 'http://lapointedesalmadies.sn/', -17.527958, 14.745585, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lapointe@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(228, 'La Fourchette', NULL, '6 rue Parchappe-Plateau', '33 821 88 87', 'http://www.fourchettedakar.com/bar_restau.html', -17.427975, 14.670455, NULL, 'restaurants', NULL, NULL, NULL, 1, 'fourchette@fourchettedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(229, 'L\'avenue', NULL, 'Avenue de la Corniche Ouest | Radisson Blu Hotel', '33 869 33 33', 'https://www.radissonblu.com/en/hotel-dakar/restaurants#/lavenue-restaurant', -17.47339, 14.697042, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info.dakar@radissonblu.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(230, 'Marina Bay', NULL, 'Route De L\'Aeroport | N\'gor Almadies, Rond point du Casino du Cap-Vert', ' 33 820 03 30', 'http://www.marinabay.sn/', -17.502343, 14.752506, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@marinabay.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(231, 'Le Galuchat', NULL, 'Radisson Blu, Hotel Dakar Sea Plaza', '33 869 33 53', 'https://www.radissonblu.com/fr/hotel-dakar/restaurants', -17.524023, 14.747459, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info.dakar@radissonblu.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(232, 'Les Jardins de l\'Ocean ', NULL, 'Mridien Prsident Almadies Route des Almadies, Dakar', '33 869 69 69', 'http://www.kingfahdpalacehotels.com/v2/en/restaurants-bars/les-jardins-de-locean/', -17.524057, 14.746969, NULL, 'restaurants', NULL, NULL, NULL, 1, 'informations.dakar@kingfahdpalacehotels.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(233, 'La Pointe des Almadies ', NULL, 'Pointe des Almadies-Almadies', '33 820 01 40', 'http://lapointedesalmadies.sn/', -17.528049, 14.745513, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lapointe@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(234, 'Noflaye Beach', NULL, 'Corniche des Almadies, Ngor, Senegal', '33 820 30 32/77 6345718', '', -17.521879, 14.741283, NULL, 'restaurants', NULL, NULL, NULL, 1, 'oumtra@hotmail.fr', NULL, NULL, NULL, 1, '', NULL, NULL),
(235, 'La Piazzola', NULL, 'Villa 86 Sotrac-Mermoz 1 rue apres le dpot DDD', '33 860 36 14', 'https://lapiazzolasn.com/', -17.479745, 14.70855, NULL, 'restaurants', NULL, NULL, NULL, 1, 'jidablade@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(236, 'Red Spoon ', NULL, 'Au Red Game, Sea Plaza Corniche Ouest', '33 859 91 90', 'http://redgames.sn/category/red-games/redspoon/', -17.474716, 14.693149, NULL, 'restaurants', NULL, NULL, NULL, 1, 'redgames@redgames.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(237, 'Sharky\'s', NULL, 'Corniche almadies, Rue ng-172, Dakar', '77 326 21 56', 'http://sharkys.sn/', -17.520351, 14.740674, NULL, 'restaurants', NULL, NULL, NULL, 1, 'hello@sharkys.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(238, 'Teranga Lounge by Pullman', NULL, '10 rue Colbert, Place de l\'Independance au Pullman', '78 631 82 82', 'https://www.accorhotels.com/gb/hotel-0563-pullman-dakar-teranga/index.shtml', -17.430782, 14.667953, NULL, 'restaurants', NULL, NULL, NULL, 1, 'dakar.reservation@accor.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(239, 'Time\'s Caf', NULL, '27 avenue Leopold Sedar Senghor', '33 821 21 68', '', -17.432716, 14.666531, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(240, 'Z\'grill', NULL, 'Point E, Boulevard de l\'Est ( ct du Lgm)', '33 825 50 52', '', -17.456854, 14.696819, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(241, 'Bidew', NULL, '89 Rue Joseph Gomis x Carnot, Dakar, Senegal', '33 823 03 20', '', -17.435501, 14.668143, NULL, 'restaurants', NULL, NULL, NULL, 1, 'restaurantlebideew@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(242, 'Bayekou', NULL, 'Parking embarcadre de Ngor-Ngor', '77 681 38 88', '', -17.511015, 14.749825, NULL, 'restaurants', NULL, NULL, NULL, 1, 'bayekoudk@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(243, 'Churrascaria Brasil', NULL, '7 Rue Filla Fann-Hock, Dakar, Senegal', '33 821 00 32', '', -17.463047, 14.679685, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(244, 'Redstone', NULL, '21, route des Almadies, Dakar 12000', '33 820 20 55', 'https://www.redstonedakar.com/', -17.521332, 14.743925, NULL, 'restaurants', NULL, NULL, NULL, 1, 'redstone@redstonedakar.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(245, 'Sao Brasil', NULL, 'Route de Ngor, Dakar, Senegal', '33 820 09 41', '', -17.507792, 14.748994, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(246, 'Simone Caf', NULL, '#7603, Villa Nabou, Rue 3, Dakar', '33 825 11 98', 'http://simonecafe.com/fr/restaurant/', -17.473489, 14.706277, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@simonecafe.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(247, 'Esprit Sushi', NULL, 'Immeuble Goeland, Boulevard El Hadj Djily Mbaye, Dakar', '33 821 70 00', '', -17.432855, 14.673344, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(248, 'Le Jardin Thailandais', NULL, 'Boulevard de Ziguinchor, Dakar', '33 825 58 33', 'https://le-jardin-thailandais.business.site/', -17.459905, 14.694516, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lejardinthailandais@yahoo.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(249, 'O sushi Bar', NULL, 'Route de la Corniche O, Dakar', '33 820 54 44', 'http://www.osushibar.com/', -17.51012, 14.739038, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(250, 'SAVEURS D\'ASIE BOURGUIBA', NULL, 'Boulevard du President Habib Bourguiba', '33 824 56 62', 'http://www.saveursdasie-sn.com/', -17.46169, 14.706062, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(251, 'SAVEURS D\'ASIE SEA PLAZA', NULL, 'Fann', '33 824 88 08', 'http://www.saveursdasie-sn.com/', -17.474437, 14.694071, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(252, 'SAVEURS D\'ASIE NGOR', NULL, 'Route de l?Aroport', '33 820 66 05', 'http://www.saveursdasie-sn.com/', -17.509474, 14.746957, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(253, 'SAVEURS D\'ASIE PLATEAU', NULL, 'Rue de Thann', '33 821 47 74', 'http://www.saveursdasie-sn.com/', -17.4314389, 14.6713156, NULL, 'restaurants', NULL, NULL, NULL, 1, 'saveurdasie@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(254, 'Sharky\'s', NULL, 'Corniche almadies, Rue ng-172, Dakar', '77 326 21 56', 'http://sharkys.sn/', -17.520383, 14.740902, NULL, 'restaurants', NULL, NULL, NULL, 1, 'hello@sharkys.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(255, 'Le Colorado', NULL, 'Boulevard de l\'est xRue de Kaolack,Point E', '76 644 64 04', 'https://www.lecolorado.com/', -17.457986, 14.695148, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@lecolorado.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(256, 'Ciao Italia', NULL, '21, Rue Huart, Dakar', '33 842 32 22', '', -17.430044, 14.671404, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(257, 'La Piazzola', NULL, 'Mermoz route de Ouakam | prêt des bus DDD', '33 860 36 14', 'https://lapiazzolasn.com/', -17.479743, 14.708768, NULL, 'restaurants', NULL, NULL, NULL, 1, 'jidablade@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(258, 'La Calebasse', NULL, 'Mamelles, Route des Almadies', '33 860 69 47', '', -17.502432, 14.72756, NULL, 'restaurants', NULL, NULL, NULL, 1, 'restaurantlacalebasse@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(259, 'Heritage', NULL, 'Boulevard du President Habib Bourguiba', '33 824 05 06', '', -17.464841, 14.701864, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(260, 'Just 4 U', NULL, 'Avenue Cheikh Anta Diop, Dakar', '33 825 99 25', '', -17.460301, 14.692071, NULL, 'restaurants', NULL, NULL, NULL, 1, 'justdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(261, 'Le Thiof', NULL, 'Sicap Baobab, Rue 9 bis', '33 824 62 07', '', -17.465271, 14.70725, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lethiof@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(262, 'Le Xalam', NULL, 'Virage, Yoff, Dakar', '77 605 32 98', 'https://www.facebook.com/Le-Xalam-772947462773814/', -17.491733, 14.755826, NULL, 'restaurants', NULL, NULL, NULL, 1, 'papispa.sylla5@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(263, 'Yuma', NULL, 'Amiti 2, Alles Seydou Nourou Tall de Citydia', '33 859 25 05', 'http://www.yumarestaurant.com/', -17.461982, 14.7036, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@yumarestaurant.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(264, 'Annapurna India', NULL, 'Route de Ngor, en face du stade', '77 394 15 90', 'http://www.annapurnadakar.com', -17.509788, 14.746099, NULL, 'restaurants', NULL, NULL, NULL, 1, 'sandeepmanmati@yahoo.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(265, 'Le Maharajah', NULL, 'Route de l\'Aeroport entre Ngor et Virage', '76 606 17 27', '', -17.494046, 14.75443, NULL, 'restaurants', NULL, NULL, NULL, 1, 'maharajadakar123@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(266, 'Chick\'N Corner', NULL, '36, Rue Victor Hugo, Dakar 23325', '33 829 28 28', 'http://the-chickn-corner.business.site/', -17.435703, 14.666135, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@thechickncorner.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(267, 'La Galette ', NULL, '16, Av Pompidou, centre ville', '33 823 63 63', '', -17.433413, 14.669935, NULL, 'restaurants', NULL, NULL, NULL, 1, 'lagalette@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(268, 'LGM- Le Glacier Moderne', NULL, 'Boulevard de l\'Est x rue 4 / POINT E', '33 825 54 54', '', -17.457102, 14.69681, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(269, 'Eric Kayser', NULL, '3, Boulevard de la Rpublique', '33 822 81 80', 'http://www.maison-kayser.com/fr/', -17.434322, 14.66495, NULL, 'restaurants', NULL, NULL, NULL, 1, 'contact@maison-kayser.com\nerickayserdakar@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(270, 'Feel Juice', NULL, '88, rue Flix Faure, Dakar', '33 821 54 00', '', -17.438351, 14.667291, NULL, 'restaurants', NULL, NULL, NULL, 1, 'feeljuice@orange.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(271, 'CI GUSTA', NULL, '143, Avenue Lamine Gueye, Dakar BP 4399', '33 821 96 37', 'https://www.cigusta.com/stores/372/', -17.436926, 14.666341, NULL, 'restaurants', NULL, NULL, NULL, 1, 'info@cigusta.com\nlouna.hoballah@cigusta.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(272, 'Patisserie Preira', NULL, 'SC-152 iba nbaye diadji', '33 824 07 53', '', -17.465664, 14.71399, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL),
(273, 'Yougurtlandia', NULL, 'Rue MZ-208, Dakar', '33 860 88 89', 'http://www.yogurtlandia.com/', -17.480089, 14.708368, NULL, 'restaurants', NULL, NULL, NULL, 1, 'yogurtlandiadkr@gmail.com', NULL, NULL, NULL, 1, '', NULL, NULL),
(274, 'N\'ice Cream', NULL, '97 Avenue Andr Peytavin. Immeuble Kb, Dakar', '33 823 35 45', 'http://www.nicecream.sn/', -17.440304, 14.669055, NULL, 'restaurants', NULL, NULL, NULL, 1, 'nicecream@arc.sn', NULL, NULL, NULL, 1, '', NULL, NULL),
(275, '', NULL, '', '', '', NULL, 0, NULL, 'restaurants', NULL, NULL, NULL, 1, '', NULL, NULL, NULL, 1, '', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `institution_galeries`
--

DROP TABLE IF EXISTS `institution_galeries`;
CREATE TABLE IF NOT EXISTS `institution_galeries` (
  `institution_id` int(11) NOT NULL,
  `galerie_id` int(11) NOT NULL,
  PRIMARY KEY (`institution_id`,`galerie_id`),
  UNIQUE KEY `UNIQ_F5FA2DC4825396CB` (`galerie_id`),
  KEY `IDX_F5FA2DC410405986` (`institution_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `institution_galeries`
--

INSERT INTO `institution_galeries` (`institution_id`, `galerie_id`) VALUES
(2, 1),
(1, 2);

-- --------------------------------------------------------

--
-- Structure de la table `institution_photo`
--

DROP TABLE IF EXISTS `institution_photo`;
CREATE TABLE IF NOT EXISTS `institution_photo` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_institution` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `date_creation` datetime NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_F5AF1FD0228D5512` (`id_institution`),
  KEY `IDX_F5AF1FD06B3CA4B` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `interest`
--

DROP TABLE IF EXISTS `interest`;
CREATE TABLE IF NOT EXISTS `interest` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `photo_bis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `interests_events`
--

DROP TABLE IF EXISTS `interests_events`;
CREATE TABLE IF NOT EXISTS `interests_events` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_interest` int(11) DEFAULT NULL,
  `id_event` int(11) DEFAULT NULL,
  `heure_debut` time NOT NULL,
  `heure_fin` time NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_B17B01E3CE5F6F2` (`id_interest`),
  KEY `IDX_B17B01ED52B4B97` (`id_event`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `interests_user`
--

DROP TABLE IF EXISTS `interests_user`;
CREATE TABLE IF NOT EXISTS `interests_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_interest` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_447882946B3CA4B` (`id_user`),
  KEY `IDX_447882943CE5F6F2` (`id_interest`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `jb_filehistory`
--

DROP TABLE IF EXISTS `jb_filehistory`;
CREATE TABLE IF NOT EXISTS `jb_filehistory` (
  `file_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`file_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `jb_filehistory`
--

INSERT INTO `jb_filehistory` (`file_name`, `original_name`, `type`, `user_id`) VALUES
('5b62fa3eec892.png', 'ma-signature.png', 'gallery', 1),
('5b62fa766374e.jpg', 'naissance.jpg', 'gallery', 1),
('5b62fa9edb521.jpg', 'chauffaar.jpg', 'gallery', 1),
('5b62fb4c3c135.jpg', 'naissance.jpg', 'gallery', 1),
('5b62fc147085c.jpg', 'naissance.jpg', 'gallery', 1),
('5b62fc81eda38.jpg', 'chauffaar.jpg', 'gallery', 1),
('5b62fe860840d.jpg', 'naissance.jpg', 'gallery', 1),
('5b62fea576f59.jpg', 'naissance.jpg', 'gallery', 1),
('5b63015525c4c.jpg', 'naissance.jpg', 'gallery', 1),
('5b63024cd34ca.jpg', 'naissance.jpg', 'gallery', 1),
('5b6303c59b3fa.jpg', 'naissance.jpg', 'gallery', 1),
('5b6304959d6a6.jpg', 'naissance.jpg', 'gallery', 1),
('5b6307bd4a076.jpg', 'naissance.jpg', 'gallery', 1),
('5b630876797d5.jpg', 'naissance.jpg', 'gallery', 1),
('5b63089d7f19e.jpg', 'naissance.jpg', 'gallery', 1),
('5b630d473487d.jpg', 'naissance.jpg', 'gallery', 1),
('5b630d5eac549.jpg', 'naissance.jpg', 'gallery', 1),
('5b63114353da2.jpg', 'naissance.jpg', 'gallery', 1),
('5b6311daa63cc.jpg', 'naissance.jpg', 'gallery', 1),
('5b6312449675c.jpg', 'naissance.jpg', 'gallery', 1),
('5b63153c547eb.jpg', 'naissance.jpg', 'gallery', 1),
('5b63155f6406c.jpg', 'naissance.jpg', 'gallery', 1),
('5b6315983dc29.jpg', 'naissance.jpg', 'gallery', 1),
('5b63178a9b5b9.jpg', 'naissance.jpg', 'gallery', 1),
('5b6317a39c0cb.jpg', 'naissance.jpg', 'gallery', 1),
('5b632269823f7.jpg', 'naissance.jpg', 'gallery', 1),
('5b63229053f3f.jpg', 'naissance.jpg', 'gallery', 1),
('5b632a4d3d4e9.jpg', 'naissance.jpg', 'gallery', 1),
('5b633d144870b.jpg', 'naissance.jpg', 'gallery', 1),
('5b68892e96454.jpg', 'restaurant.jpg', 'gallery', 1),
('5b688a470ac84.jpg', 'restaurant.jpg', 'gallery', 1),
('5b688aca9f627.jpg', 'restaurant.jpg', 'gallery', 1),
('5b688b1010a8b.jpg', 'restaurant.jpg', 'gallery', 1),
('5b688ddebef81.jpg', 'restaurant.jpg', 'gallery', 1),
('5b688df3c9743.jpg', 'restaurant.jpg', 'gallery', 1),
('5b688e59a122b.jpg', 'restaurant.jpg', 'gallery', 1),
('5b688edad7509.jpg', 'restaurant.jpg', 'gallery', 1);

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `restaurant_id` int(11) DEFAULT NULL,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_7D053A93B1E7706E` (`restaurant_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `menu`
--

INSERT INTO `menu` (`id`, `restaurant_id`, `nom`, `description`, `image`) VALUES
(1, 74, 'Menu de la semaine', 'Description meni', '');

-- --------------------------------------------------------

--
-- Structure de la table `note`
--

DROP TABLE IF EXISTS `note`;
CREATE TABLE IF NOT EXISTS `note` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_event` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `id_institution` int(11) DEFAULT NULL,
  `avis` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `note` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_CFBDFA14D52B4B97` (`id_event`),
  KEY `IDX_CFBDFA146B3CA4B` (`id_user`),
  KEY `IDX_CFBDFA14228D5512` (`id_institution`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `note`
--

INSERT INTO `note` (`id`, `id_event`, `id_user`, `id_institution`, `avis`, `note`) VALUES
(1, NULL, 1, 74, 'Test', 5),
(2, NULL, 1, 74, 'nouvel avis', 4),
(3, NULL, 3, 74, 'nouvel avis', 4);

-- --------------------------------------------------------

--
-- Structure de la table `notification`
--

DROP TABLE IF EXISTS `notification`;
CREATE TABLE IF NOT EXISTS `notification` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_event` int(11) DEFAULT NULL,
  `id_institution` int(11) DEFAULT NULL,
  `id_user` int(11) DEFAULT NULL,
  `message` longtext COLLATE utf8_unicode_ci,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vue` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_BF5476CAD52B4B97` (`id_event`),
  KEY `IDX_BF5476CA228D5512` (`id_institution`),
  KEY `IDX_BF5476CA6B3CA4B` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `offre`
--

DROP TABLE IF EXISTS `offre`;
CREATE TABLE IF NOT EXISTS `offre` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `identifiant` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `offre`
--

INSERT INTO `offre` (`id`, `nom`, `description`, `identifiant`) VALUES
(1, 'Basique', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium aspernatur eaque eius facilis illum laborum nesciunt placeat possimus sit ut.', 'basic'),
(2, 'Premium', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur cum expedita impedit, magnam obcaecati quia vero? A ab, culpa delectus doloremque doloribus id in magni maxime praesentium quibusdam quisquam tenetur?', 'premium'),
(3, 'Gold', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Consequatur cum expedita impedit, magnam obcaecati quia vero? A ab, culpa delectus doloremque doloribus id in magni maxime praesentium quibusdam quisquam tenetur?', 'gold');

-- --------------------------------------------------------

--
-- Structure de la table `place`
--

DROP TABLE IF EXISTS `place`;
CREATE TABLE IF NOT EXISTS `place` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `adresse` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `latitude` double DEFAULT NULL,
  `longitude` double DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `place`
--

INSERT INTO `place` (`id`, `nom`, `adresse`, `latitude`, `longitude`) VALUES
(1, 'Dakar', 'Hôtel Njambour', NULL, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `publicite`
--

DROP TABLE IF EXISTS `publicite`;
CREATE TABLE IF NOT EXISTS `publicite` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `titre` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `date_publicite` datetime DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_1D394E396B3CA4B` (`id_user`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `sous_categorie`
--

DROP TABLE IF EXISTS `sous_categorie`;
CREATE TABLE IF NOT EXISTS `sous_categorie` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` longtext COLLATE utf8_unicode_ci,
  `image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `id_category` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_52743D7B5697F554` (`id_category`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `sous_categorie`
--

INSERT INTO `sous_categorie` (`id`, `nom`, `description`, `image`, `id_category`) VALUES
(1, 'Fast Food', NULL, NULL, 1),
(2, 'Pâtisserie / Glacier', NULL, NULL, 1),
(3, 'Habillement', NULL, NULL, 6),
(4, 'Chaussures et accessoire', NULL, NULL, 6),
(5, 'Bijouterie', NULL, NULL, 6),
(6, 'Esthétique', NULL, NULL, 3),
(7, 'Couture', NULL, NULL, 3),
(8, 'Soin', NULL, NULL, 2),
(9, 'Sport et Fitness', NULL, NULL, 2);

-- --------------------------------------------------------

--
-- Structure de la table `tag`
--

DROP TABLE IF EXISTS `tag`;
CREATE TABLE IF NOT EXISTS `tag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `tag_decouverte`
--

DROP TABLE IF EXISTS `tag_decouverte`;
CREATE TABLE IF NOT EXISTS `tag_decouverte` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `article_id` int(11) DEFAULT NULL,
  `event_id` int(11) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `IDX_AB880AA57294869C` (`article_id`),
  KEY `IDX_AB880AA571F7E88B` (`event_id`),
  KEY `IDX_AB880AA5A76ED395` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `tag_decouverte`
--

INSERT INTO `tag_decouverte` (`id`, `nom`, `photo`, `article_id`, `event_id`, `user_id`) VALUES
(3, 'Business & economie', NULL, NULL, NULL, NULL),
(4, 'Art', NULL, NULL, NULL, NULL),
(5, 'Science & Technologie', NULL, NULL, NULL, NULL),
(6, 'Sante & bien Etre', NULL, NULL, NULL, NULL),
(7, 'Voyage', NULL, NULL, NULL, NULL),
(8, 'Activite de plein aire', NULL, NULL, NULL, NULL),
(9, 'Politique', NULL, NULL, NULL, NULL),
(10, 'Celebrite', NULL, NULL, NULL, NULL),
(11, 'Recette culinaire', NULL, NULL, NULL, NULL),
(12, 'Design', NULL, NULL, NULL, NULL),
(13, 'Mode & Beaute', NULL, NULL, NULL, NULL),
(14, 'Style', NULL, NULL, NULL, NULL),
(15, 'Festival de musique', NULL, NULL, NULL, NULL),
(16, 'Automobiles, bateaux et Avions', NULL, NULL, NULL, NULL),
(17, 'Lecture', NULL, NULL, NULL, NULL),
(18, 'Concerts et Spectacle', NULL, NULL, NULL, NULL),
(19, 'Evenements professionels', NULL, NULL, NULL, NULL),
(20, 'Famille et education', NULL, NULL, NULL, NULL),
(21, 'Gastronomie', NULL, NULL, NULL, NULL),
(22, 'Oeuvres carritatives', NULL, NULL, NULL, NULL),
(23, 'Comédie', NULL, NULL, NULL, NULL),
(24, 'Maison et Mode de vie', NULL, NULL, NULL, NULL),
(25, 'Night Life', NULL, NULL, NULL, NULL),
(26, 'Photographie', NULL, NULL, NULL, NULL),
(27, 'Religion et Spiritualite', NULL, NULL, NULL, NULL),
(28, 'Pop Culture', NULL, NULL, NULL, NULL),
(29, 'Musique', NULL, NULL, NULL, NULL),
(30, 'Films et divertissement', NULL, NULL, NULL, NULL),
(31, 'Bio', NULL, NULL, NULL, NULL),
(32, 'Television', NULL, NULL, NULL, NULL),
(33, 'Cinéma', NULL, NULL, NULL, NULL),
(34, 'Leadership', NULL, NULL, NULL, NULL),
(35, 'Entrepreneuriat', NULL, NULL, NULL, NULL),
(36, 'Jeux Vidéos', NULL, NULL, NULL, NULL);

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
  ADD CONSTRAINT `FK_23A0E665697F554` FOREIGN KEY (`id_category`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FK_23A0E666B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `composant`
--
ALTER TABLE `composant`
  ADD CONSTRAINT `FK_EC8486C9CCD7E912` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`id`);

--
-- Contraintes pour la table `event`
--
ALTER TABLE `event`
  ADD CONSTRAINT `FK_3BAE0AA7228D5512` FOREIGN KEY (`id_institution`) REFERENCES `institution` (`id`),
  ADD CONSTRAINT `FK_3BAE0AA75697F554` FOREIGN KEY (`id_category`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FK_3BAE0AA76B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `event_photo`
--
ALTER TABLE `event_photo`
  ADD CONSTRAINT `FK_55AC3534D52B4B97` FOREIGN KEY (`id_event`) REFERENCES `event` (`id`);

--
-- Contraintes pour la table `event_tag`
--
ALTER TABLE `event_tag`
  ADD CONSTRAINT `FK_1246725071F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `FK_12467250BAD26311` FOREIGN KEY (`tag_id`) REFERENCES `tag` (`id`) ON DELETE CASCADE;

--
-- Contraintes pour la table `institution`
--
ALTER TABLE `institution`
  ADD CONSTRAINT `FK_3A9F98E54CC8505A` FOREIGN KEY (`offre_id`) REFERENCES `offre` (`id`),
  ADD CONSTRAINT `FK_3A9F98E55697F554` FOREIGN KEY (`id_category`) REFERENCES `categorie` (`id`),
  ADD CONSTRAINT `FK_3A9F98E56B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_3A9F98E5B02AFCF1` FOREIGN KEY (`id_sous_category`) REFERENCES `sous_categorie` (`id`);

--
-- Contraintes pour la table `institution_galeries`
--
ALTER TABLE `institution_galeries`
  ADD CONSTRAINT `FK_F5FA2DC410405986` FOREIGN KEY (`institution_id`) REFERENCES `institution` (`id`),
  ADD CONSTRAINT `FK_F5FA2DC4825396CB` FOREIGN KEY (`galerie_id`) REFERENCES `galerie` (`id`);

--
-- Contraintes pour la table `institution_photo`
--
ALTER TABLE `institution_photo`
  ADD CONSTRAINT `FK_F5AF1FD0228D5512` FOREIGN KEY (`id_institution`) REFERENCES `institution` (`id`),
  ADD CONSTRAINT `FK_F5AF1FD06B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `interests_events`
--
ALTER TABLE `interests_events`
  ADD CONSTRAINT `FK_B17B01E3CE5F6F2` FOREIGN KEY (`id_interest`) REFERENCES `interest` (`id`),
  ADD CONSTRAINT `FK_B17B01ED52B4B97` FOREIGN KEY (`id_event`) REFERENCES `event` (`id`);

--
-- Contraintes pour la table `interests_user`
--
ALTER TABLE `interests_user`
  ADD CONSTRAINT `FK_447882943CE5F6F2` FOREIGN KEY (`id_interest`) REFERENCES `interest` (`id`),
  ADD CONSTRAINT `FK_447882946B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `FK_7D053A93B1E7706E` FOREIGN KEY (`restaurant_id`) REFERENCES `institution` (`id`);

--
-- Contraintes pour la table `note`
--
ALTER TABLE `note`
  ADD CONSTRAINT `FK_CFBDFA14228D5512` FOREIGN KEY (`id_institution`) REFERENCES `institution` (`id`),
  ADD CONSTRAINT `FK_CFBDFA146B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_CFBDFA14D52B4B97` FOREIGN KEY (`id_event`) REFERENCES `event` (`id`);

--
-- Contraintes pour la table `notification`
--
ALTER TABLE `notification`
  ADD CONSTRAINT `FK_BF5476CA228D5512` FOREIGN KEY (`id_institution`) REFERENCES `institution` (`id`),
  ADD CONSTRAINT `FK_BF5476CA6B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`),
  ADD CONSTRAINT `FK_BF5476CAD52B4B97` FOREIGN KEY (`id_event`) REFERENCES `event` (`id`);

--
-- Contraintes pour la table `publicite`
--
ALTER TABLE `publicite`
  ADD CONSTRAINT `FK_1D394E396B3CA4B` FOREIGN KEY (`id_user`) REFERENCES `fos_user` (`id`);

--
-- Contraintes pour la table `sous_categorie`
--
ALTER TABLE `sous_categorie`
  ADD CONSTRAINT `FK_52743D7B5697F554` FOREIGN KEY (`id_category`) REFERENCES `categorie` (`id`);

--
-- Contraintes pour la table `tag_decouverte`
--
ALTER TABLE `tag_decouverte`
  ADD CONSTRAINT `FK_AB880AA571F7E88B` FOREIGN KEY (`event_id`) REFERENCES `event` (`id`),
  ADD CONSTRAINT `FK_AB880AA57294869C` FOREIGN KEY (`article_id`) REFERENCES `article` (`id`),
  ADD CONSTRAINT `FK_AB880AA5A76ED395` FOREIGN KEY (`user_id`) REFERENCES `fos_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
