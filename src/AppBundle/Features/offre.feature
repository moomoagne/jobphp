Feature: Test add Offre

  Scenario: La page d'auth affiche bien les fields de nom d'utilisateur et le mot de passe
    Given I am on "/login"
    When I fill in "username" with "moomoagne"
    When I fill in "password" with "passer"
    When I press "Connexion"
    Then I should be on "/admin"
    Then I should see "Tableau de bord"
  #  Then I wait for 10 seconds
    
    Given |I am on "/admin" page

    Given I click on the element with css selector "a#3"
    When I click on the element with css selector "a#3-2"
    Then I should see "Ajouter une nouvelle offre"


    Given |I am on "/admin/offre/new" page
    When I fill in "appbundle_offre_nom" with "Offre1"
    When I fill in "appbundle_offre_identifiant" with "IdentifantOffre1"
    When I fill in "appbundle_offre_description" with "Description Offre1"
    When I press "Enregistrer"
    Then I should be on "admin/offre/4"
    Then I should see "Présentation de l'offre"