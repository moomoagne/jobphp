<?php

namespace AppBundle\Controller;

use AppBundle\Entity\BienEtre;
use AppBundle\Entity\DefaultInstitution;
use AppBundle\Entity\Galerie;
use AppBundle\Entity\Hotels;
use AppBundle\Entity\Institution;
use AppBundle\Entity\ModeBeaute;
use AppBundle\Entity\NightLife;
use AppBundle\Entity\Prestataires;
use AppBundle\Entity\Restaurants;
use AppBundle\Entity\Shopping;
use AppBundle\Entity\ThingsToDo;
use AppBundle\Form\RestaurantsType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Service\FileUploader;
use Symfony\Component\HttpFoundation\Session\Session;

/**
 * Institution controller.
 *
 * @Route("institution")
 */
class InstitutionController extends Controller
{

    /**
     * Lists all event entities.
     *
     * @Route("/{id}/{status}/{identifiant}", name="institution_approve")
     * @Method("GET")
     */
    public function approveAction($id, $status, $identifiant)
    {
        $em = $this->getDoctrine()->getManager();

        $etablissement = null;

        if($identifiant == "restaurants")
        {
            $etablissement = $em->getRepository('AppBundle:Restaurants')->find($id);

        }
        elseif ($identifiant == "bien-etre")
        {
            $etablissement = $em->getRepository('AppBundle:BienEtre')->find($id);
        }
        elseif ($identifiant == "mode-beaute")
        {
            $etablissement = $em->getRepository('AppBundle:ModeBeaute')->find($id);
        }
        elseif ($identifiant == "night-life")
        {
            $etablissement = $em->getRepository('AppBundle:NightLife')->find($id);
        }
        elseif ($identifiant == "prestataires")
        {
            $etablissement = $em->getRepository('AppBundle:Prestataires')->find($id);
        }
        elseif ($identifiant == "shopping")
        {
            $etablissement = $em->getRepository('AppBundle:Shopping')->find($id);
        }
        elseif ($identifiant == "thing-to-do")
        {
            $etablissement = $em->getRepository('AppBundle:ThingsToDo')->find($id);
        }
        elseif ($identifiant == "hotels")
        {
            $etablissement = $em->getRepository('AppBundle:Hotels')->find($id);
        }

        if($status == 1)
        {
            if($etablissement) {
                $etablissement->setStatus(1);

                // Retrieve flashbag from the controller
                $flashbag = $this->get('session')->getFlashBag();

                // Add flash message
                $flashbag->add("success_approve", "Etablissement approuvé avec succès !");

                $message = (new \Swift_Message('Salut'))
                    ->setFrom('contact@livindkr.com')
                    ->setTo($etablissement->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'Emails/etab_approve.html.twig',
                            array('etablissement' => $etablissement)
                        ),
                        'text/html'
                    )
                    /*
                     * If you also want to include a plaintext version of the message
                    ->addPart(
                        $this->renderView(
                            'Emails/registration.txt.twig',
                            array('name' => $name)
                        ),
                        'text/plain'
                    )
                    */
                ;


                $this->get('mailer')->send($message);
            }

        }elseif ($status == 0)
        {
            // Retrieve flashbag from the controller
            $flashbag = $this->get('session')->getFlashBag();

            // Add flash message
            $flashbag->add("failed_approve", "Etablissement non approuvé !");

            $etablissement->setStatus(0);
        }

        if($etablissement != null){
            $em->persist($etablissement);
            $em->flush();
            return $this->redirectToRoute('admin_institution_index');
        }
    }

    /**
     * Lists all institution entities.
     *
     * @Route("/", name="admin_institution_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $institutions = $em->getRepository('AppBundle:Institution')
            ->findBy(array(), array('id' => 'DESC'), null, null);

        $categories = $em->getRepository('AppBundle:Categorie')->findBy(array("is_entity" => true));

        return $this->render('institution/index.html.twig', array(
            'institutions' => $institutions,
            'cats' => $categories
        ));
    }

    /*/**
     * Creates a new institution entity.
     *
     * @Route("/new", name="admin_institution_new")
     * @Method({"GET", "POST"})

    public function newAction(Request $request)
    {
        $institution = new Restaurants();
        $form = $this->createForm('AppBundle\Form\Type\DefaultInstitutionType', $institution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($institution);
            $em->flush();

            return $this->redirectToRoute('admin_institution_show', array('id' => $institution->getId()));
        }

        return $this->render('institution/new.html.twig', array(
            'institution' => $institution,
            'form' => $form->createView(),
        ));
    }*/

    /**
     * Creates a new institution entity.
     *
     * @Route("/galeries/{id}", name="ajax_snippet_image_send")
     * @Method({"GET", "POST"})
     */
    public function uploadPhotoAction(Request $request, $id)
    {
        $em = $this->container->get("doctrine.orm.default_entity_manager");

        $document = new Galerie();

        $media = $request->files->get('file');

        $document->setFile($media);
        $document->setPath($media->getPathName());
        $document->setNom($media->getClientOriginalName());
        $document->upload();
        $em->persist($document);

        $institution = $em->getRepository('AppBundle:Institution')->find($id);

        if($institution->getCategorie()->getIdentifiant() == "restaurants")
        {
            $institution = $em->getRepository('AppBundle:Restaurants')->find($id);
            $institution->addGalery($document);
            $em->persist($institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "bien-etre")
        {
            $institution = $em->getRepository('AppBundle:BienEtre')->find($id);
            $institution->addGalery($document);
            $em->persist($institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "mode-beaute")
        {
            $institution = $em->getRepository('AppBundle:ModeBeaute')->find($id);
            $institution->addGalery($document);
            $em->persist($institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "night-life")
        {
            $institution = $em->getRepository('AppBundle:NightLife')->find($id);
            $institution->addGalery($document);
            $em->persist($institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "prestataires")
        {
            $institution = $em->getRepository('AppBundle:Prestataires')->find($id);
            $institution->addGalery($document);
            $em->persist($institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "shopping")
        {
            $institution = $em->getRepository('AppBundle:Shopping')->find($id);
            $institution->addGalery($document);
            $em->persist($institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "thing-to-do")
        {
            $institution = $em->getRepository('AppBundle:ThingsToDo')->find($id);
            $institution->addGalery($document);
            $em->persist($institution);
        }
        else {
            $institution->addGalery($document);
            $em->persist($institution);
        }


        $em->flush();

        //infos sur le document envoyé
        // var_dump($request->files->get('file'));die;

        return new JsonResponse(array('success' => true));
    }

    /**
     * Creates a new institution entity.
     *
     * @Route("/nouvelle", name="admin_institutions_nouvelle")
     * @Method({"GET", "POST"})
     */
    public function createInstitutionAction(FileUploader $fileUploader, Request $request)
    {

        $session = $request->getSession();;
        $session->start();

        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AppBundle:Categorie')->findBy(array('is_entity' => true));


        $formData = new DefaultInstitution();
        $flow = $this->get('appbundle.form.flow.createInstittution'); // must match the flow's service id
        $flow->bind($formData);


        // form of the current step
        $form = $flow->createForm();

        if ($flow->isValid($form)) {

            if($request->get('sous_categorie')){
                $sous_cat = $em->getRepository('AppBundle:SousCategorie')->find($request->get('sous_categorie'));
                // dump($form) ;
                $session->set('sous_cat', $sous_cat);
            }

            $flow->saveCurrentStepData($form);

            if ($flow->nextStep()) {
                // form for the next step
                $form = $flow->createForm();

                $formData->setStatus(2);

                // dump($formData) ;

                /*if(in_array($formData->getCategorie(), $categories, true))
                {
                    $form=$flow->createForm(RestaurantsType::class);
                }*/
                // $flow->saveCurrentStepData($form);

            } else {

                //Upload photo institut
                if($formData->getPhoto())
                {
                    // $file stores the uploaded PDF file
                    /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                    $file = $formData->getPhoto();

                    $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                    // moves the file to the directory where brochures are stored
                    $file->move(
                        $this->getParameter('photo_directory'),
                        $fileName
                    );

                    // updates the 'brochure' property to store the PDF file name
                    // instead of its contents
                    $formData->setPhoto($fileName);

                }

                // si le type est une classe défini
                if(in_array($formData->getCategorie(), $categories, true)) {

                    $formDataNew = $this->setEntity($formData, $session->get('sous_cat'));
                    //ajouter à l'utilisateur courant
                    $formDataNew->setUser($this->getUser());

                    $em->persist($formDataNew);
                    $em->flush();

                    // Retrieve flashbag from the controller
                    $flashbag = $this->get('session')->getFlashBag();

                    // Add flash message
                    $flashbag->add("success", "Votre point d'intérêt à été ajouté avec succès !");

                }else {

                    //ajouter à l'utilisateur courant
                    $formData->setUser($this->getUser());

                    $em->persist($formData);
                    $em->flush();
                }

                $flow->reset(); // remove step data from the session
                $session->clear();

                return $this->redirect($this->generateUrl('admin_institution_index')); // redirect when done
            }
        }

        return $this->render('institution/createInstitution.html.twig', array(
            'form' => $form->createView(),
            'flow' => $flow,
            'categories' => $categories,
        ));
    }

    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }

    public function setEntity($formData, $sous_cat)
    {
        $formDataNew = null;

        if($formData->getCategorie()->getIdentifiant() == "restaurants")
        {
            $formDataNew = new Restaurants();
            $formDataNew->setPromo($formData->getPromo());

        }
        elseif ($formData->getCategorie()->getIdentifiant() == "bien-etre")
        {
            $formDataNew = new BienEtre();
        }
        elseif ($formData->getCategorie()->getIdentifiant() == "mode-beaute")
        {
            $formDataNew = new ModeBeaute();
        }
        elseif ($formData->getCategorie()->getIdentifiant() == "night-life")
        {
            $formDataNew = new NightLife();
        }
        elseif ($formData->getCategorie()->getIdentifiant() == "prestataires")
        {
            $formDataNew = new Prestataires();
        }
        elseif ($formData->getCategorie()->getIdentifiant() == "shopping")
        {
            $formDataNew = new Shopping();
        }
        elseif ($formData->getCategorie()->getIdentifiant() == "thing-to-do")
        {
            $formDataNew = new ThingsToDo();
        }
        elseif ($formData->getCategorie()->getIdentifiant() == "hotels")
        {
            $formDataNew = new Hotels();
        }
        $formDataNew->setNom($formData->getNom());
        $formDataNew->setDescription($formData->getDescription());
        $formDataNew->setAdresse($formData->getAdresse());
        $formDataNew->setTelephone($formData->getTelephone());
        $formDataNew->setPrice($formData->getPrice());
        $formDataNew->setLongitude($formData->getLongitude());
        $formDataNew->setLatitude($formData->getLatitude());
        $formDataNew->setPhoto($formData->getPhoto());
        $formDataNew->setOffre($formData->getOneOffre());
        $formDataNew->setCategorie($formData->getCategorie());

        if($sous_cat) {
            $sous_cat = $this->getDoctrine()->getManager()
                ->getRepository('AppBundle:SousCategorie')->find($sous_cat->getId());
        }

        $formDataNew->setSousCategorie($sous_cat);
        $formDataNew->setStatus($formData->getStatus());

        return $formDataNew;
    }

    /**
     * Finds and displays a institution entity.
     *
     * @Route("/{id}", name="admin_institution_show")
     * @Method("GET")
     */
    public function showAction(Institution $institution)
    {
        $deleteForm = $this->createDeleteForm($institution);

        return $this->render('institution/show.html.twig', array(
            'institution' => $institution,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing institution entity.
     *
     * @Route("/{id}/edit", name="admin_institution_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Institution $institution)
    {
        $deleteForm = $this->createDeleteForm($institution);

        $editForm = null;

        if($institution->getCategorie()->getIdentifiant() == "restaurants")
        {
            $editForm = $this->createForm('AppBundle\Form\RestaurantsType', $institution);

        }
        elseif ($institution->getCategorie()->getIdentifiant() == "bien-etre")
        {
            $editForm = $this->createForm('AppBundle\Form\BienEtreType', $institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "mode-beaute")
        {
            $editForm = $this->createForm('AppBundle\Form\ModeBeauteType', $institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "night-life")
        {
            $editForm = $this->createForm('AppBundle\Form\NightLifeType', $institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "prestataires")
        {
            $editForm = $this->createForm('AppBundle\Form\PrestatairesType', $institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "shopping")
        {
            $editForm = $this->createForm('AppBundle\Form\ShoppingType', $institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "thing-to-do")
        {
            $editForm = $this->createForm('AppBundle\Form\ThingToDoType', $institution);
        }
        elseif ($institution->getCategorie()->getIdentifiant() == "hotels")
        {
            $editForm = $this->createForm('AppBundle\Form\HotelsType', $institution);
        }

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_institution_show', array('id' => $institution->getId()));
        }

        return $this->render('institution/edit.html.twig', array(
            'institution' => $institution,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a institution entity.
     *
     * @Route("/{id}", name="admin_institution_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Institution $institution)
    {
        $form = $this->createDeleteForm($institution);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($institution);
            $em->flush();
        }

        return $this->redirectToRoute('admin_institution_index');
    }

    /**
     * Creates a form to delete a institution entity.
     *
     * @param Institution $institution The institution entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Institution $institution)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_institution_delete', array('id' => $institution->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
