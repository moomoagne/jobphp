<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class AdminController extends Controller
{
    /**
     * @Route("/admin", name="admin_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();


        $institutions = $em->getRepository('AppBundle:Institution')->findAll();
        $events = $em->getRepository('AppBundle:Event')->findAll();
        $articles = $em->getRepository('AppBundle:Article')->findAll();
        $publicites = $em->getRepository('AppBundle:Publicite')->findAll();

        return $this->render('AppBundle:Admin:index.html.twig', array(
           'institutions' => $institutions,
            'events' => $events,
            'articles' => $articles,
            'publicites' => $publicites
        ));
    }

}
