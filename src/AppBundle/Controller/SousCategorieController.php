<?php

namespace AppBundle\Controller;

use AppBundle\Entity\SousCategorie;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

/**
 * Souscategorie controller.
 *
 * @Route("admin/souscategorie")
 */
class SousCategorieController extends Controller
{
    /**
     * Lists all sousCategorie entities.
     *
     * @Route("/", name="admin_souscategorie_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $sousCategories = $em->getRepository('AppBundle:SousCategorie')->findAll();

        return $this->render('souscategorie/index.html.twig', array(
            'sousCategories' => $sousCategories,
        ));
    }

    /**
     * Creates a new sousCategorie entity.
     *
     * @Route("/new", name="admin_souscategorie_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $sousCategorie = new Souscategorie();
        $form = $this->createForm('AppBundle\Form\SousCategorieType', $sousCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            if($sousCategorie->getImage())
            {
                // $file stores the uploaded PDF file
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $sousCategorie->getImage();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('categorie_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $sousCategorie->setImage($fileName);
            }

            $em = $this->getDoctrine()->getManager();
            $em->persist($sousCategorie);
            $em->flush();

            // Retrieve flashbag from the controller
            $flashbag = $this->get('session')->getFlashBag();

            // Add flash message
            $flashbag->add("success", "La sous catégorie '".$sousCategorie->getNom()."' ajoutée avec succès !");

            return $this->redirectToRoute('admin_souscategorie_index');
        }

        return $this->render('souscategorie/new.html.twig', array(
            'sousCategorie' => $sousCategorie,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a sousCategorie entity.
     *
     * @Route("/{id}", name="admin_souscategorie_show")
     * @Method("GET")
     */
    public function showAction(SousCategorie $sousCategorie)
    {
        $deleteForm = $this->createDeleteForm($sousCategorie);

        return $this->render('souscategorie/show.html.twig', array(
            'sousCategorie' => $sousCategorie,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing sousCategorie entity.
     *
     * @Route("/{id}/edit", name="admin_souscategorie_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, SousCategorie $sousCategorie)
    {
        $filename = null;

        if ($sousCategorie->getImage())
        {
            $filename = $sousCategorie->getImage();
            $sousCategorie->setImage(
                new File($this->getParameter('categorie_directory'). '/'. $sousCategorie->getImage())
            );
        }

        $deleteForm = $this->createDeleteForm($sousCategorie);
        $editForm = $this->createForm('AppBundle\Form\SousCategorieType', $sousCategorie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if($filename != null) {
                $sousCategorie->setImage($filename) ;
            }


            if($sousCategorie->getImage() && $sousCategorie->getImage() != $filename)
            {
                // $file stores the uploaded PDF file
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $sousCategorie->getImage();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('categorie_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $sousCategorie->setImage($fileName);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('admin_souscategorie_edit', array('id' => $sousCategorie->getId()));
        }

        return $this->render('souscategorie/edit.html.twig', array(
            'sousCategorie' => $sousCategorie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a sousCategorie entity.
     *
     * @Route("/{id}", name="admin_souscategorie_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, SousCategorie $sousCategorie)
    {
        $form = $this->createDeleteForm($sousCategorie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($sousCategorie);
            $em->flush();
        }

        return $this->redirectToRoute('admin_souscategorie_index');
    }

    /**
     * Creates a form to delete a sousCategorie entity.
     *
     * @param SousCategorie $sousCategorie The sousCategorie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SousCategorie $sousCategorie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_souscategorie_delete', array('id' => $sousCategorie->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
