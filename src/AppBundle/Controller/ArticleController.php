<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\Request;

/**
 * Article controller.
 *
 * @Route("article")
 */
class ArticleController extends Controller
{
    /**
     * Lists all article entities.
     *
     * @Route("/", name="article_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $articles = $em->getRepository('AppBundle:Article')->findAll();

        return $this->render('article/index.html.twig', array(
            'articles' => $articles,
        ));
    }

    /**
     * Lists all event entities.
     *
     * @Route("/approve/{id}/{status}", name="article_approve")
     * @Method("GET")
     */
    public function approveAction($id, $status)
    {
        $em = $this->getDoctrine()->getManager();

        $article = $em->getRepository('AppBundle:Article')->find($id);

        if($status == 1)
        {
            $article->setStatus(1);

            if($article) {
                $message = (new \Swift_Message('Salut '))
                    ->setFrom('contact@livindkr.com')
                    ->setTo($this->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'Emails/article_approve_success.html.twig',
                            array('article' => $article)
                        ),
                        'text/html'
                    )
                    /*
                     * If you also want to include a plaintext version of the message
                    ->addPart(
                        $this->renderView(
                            'Emails/registration.txt.twig',
                            array('name' => $name)
                        ),
                        'text/plain'
                    )
                    */
                ;


                $this->get('mailer')->send($message);
            }
            // Retrieve flashbag from the controller
            $flashbag = $this->get('session')->getFlashBag();

            // Add flash message
            $flashbag->add("success_approve", "Article approuvé et publié avec succès !");

        }elseif ($status == 0)
        {
            $article->setStatus(0);

            //send mail

            if($article) {
                $message = (new \Swift_Message('Salut '))
                    ->setFrom('contact@livindkr.com')
                    ->setTo($this->getUser()->getEmail())
                    ->setBody(
                        $this->renderView(
                        // app/Resources/views/Emails/registration.html.twig
                            'Emails/article_approve_failed.html.twig',
                            array('article' => $article)
                        ),
                        'text/html'
                    )
                    /*
                     * If you also want to include a plaintext version of the message
                    ->addPart(
                        $this->renderView(
                            'Emails/registration.txt.twig',
                            array('name' => $name)
                        ),
                        'text/plain'
                    )
                    */
                ;


                $this->get('mailer')->send($message);
            }

            // Retrieve flashbag from the controller
            $flashbag = $this->get('session')->getFlashBag();

            // Add flash message
            $flashbag->add("success_approve", "Evénement non approuvé !");
        }

        $em->persist($article);
        $em->flush();

        return $this->redirectToRoute('article_index');
    }


    /**
     * Creates a new article entity.
     *
     * @Route("/new", name="article_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $article = new Article();
        $form = $this->createForm('AppBundle\Form\ArticleType', $article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            if($article->getImage()){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $article->getImage();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $article->setImage($fileName);
            }

            $category = $em->getRepository('AppBundle:Categorie')->findOneBy(array('identifiant'=> 'decouverte'));

            $article->setCategorie($category);

            $article->setNbLecteur(0);
            $article->setStatus(2);


            $em->persist($article);
            $em->flush();

            // Retrieve flashbag from the controller
            $flashbag = $this->get('session')->getFlashBag();

            // Add flash message
            $flashbag->add("success_approve", "Article ajouté avec succès et est encours d'approvation !");

            return $this->redirectToRoute('article_index');
        }

        return $this->render('article/new.html.twig', array(
            'article' => $article,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a article entity.
     *
     * @Route("/{id}", name="article_show")
     * @Method("GET")
     */
    public function showAction(Article $article)
    {
        $deleteForm = $this->createDeleteForm($article);

        return $this->render('article/show.html.twig', array(
            'article' => $article,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing article entity.
     *
     * @Route("/{id}/edit", name="article_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Article $article)
    {
        $filename = null;

        if ($article->getImage())
        {
            $filename = $article->getImage();
            $article->setImage(new File($this->getParameter('articles_directory'). '/'. $article->getImage()));
        }

        $deleteForm = $this->createDeleteForm($article);
        $editForm = $this->createForm('AppBundle\Form\ArticleType', $article);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {

            if($filename != null) {
                $article->setImage($filename) ;
            }

            if($article->getImage() && $article->getPhoto() != $filename){
                /** @var Symfony\Component\HttpFoundation\File\UploadedFile $file */
                $file = $article->getImage();

                $fileName = $this->generateUniqueFileName().'.'.$file->guessExtension();

                // moves the file to the directory where brochures are stored
                $file->move(
                    $this->getParameter('articles_directory'),
                    $fileName
                );

                // updates the 'brochure' property to store the PDF file name
                // instead of its contents
                $article->setImage($fileName);
            }

            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('article_edit', array('id' => $article->getId()));
        }

        return $this->render('article/edit.html.twig', array(
            'article' => $article,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a article entity.
     *
     * @Route("/{id}", name="article_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Article $article)
    {
        $form = $this->createDeleteForm($article);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($article);
            $em->flush();
        }

        return $this->redirectToRoute('article_index');
    }

    /**
     * Creates a form to delete a article entity.
     *
     * @param Article $article The article entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Article $article)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('article_delete', array('id' => $article->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }


    /**
     * @return string
     */
    private function generateUniqueFileName()
    {
        // md5() reduces the similarity of the file names generated by
        // uniqid(), which is based on timestamps
        return md5(uniqid());
    }
}
