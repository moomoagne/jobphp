<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Restaurants
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RestaurantsRepository")
 */
class Restaurants extends Institution
{

//    /**
//     * One Instituts has Many Menus.
//     * @ORM\OneToMany(targetEntity="Menu", mappedBy="restaurant")
//     */
//    private $menus;

    /**
     * Many User have Many Menu.
     * @ORM\ManyToMany(targetEntity="Menu", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="restaurants_menus",
     *      joinColumns={@ORM\JoinColumn(name="restaurants_id", referencedColumnName="id", nullable=true)},
     *      inverseJoinColumns={@ORM\JoinColumn(name="menu_id", referencedColumnName="id", unique=false, nullable=true)}
     *      )
     */
    private $menus;


    public function __construct()
    {
        $this->menus = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add menu
     *
     * @param \AppBundle\Entity\Menu $menu
     *
     * @return Restaurants
     */
    public function addMenu(\AppBundle\Entity\Menu $menu)
    {
        $this->menus[] = $menu;

        return $this;
    }

    /**
     * Remove menu
     *
     * @param \AppBundle\Entity\Menu $menu
     */
    public function removeMenu(\AppBundle\Entity\Menu $menu)
    {
        $this->menus->removeElement($menu);
    }

    /**
     * Get menus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMenus()
    {
        return $this->menus;
    }
}
