<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Shopping
 *
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ShoppingRepository")
 */
class Shopping extends Institution
{

    /**
     * @var bool
     *
     * @ORM\Column(name="solde", type="boolean")
     */
    private $solde;


    /**
     * Set solde
     *
     * @param boolean $solde
     *
     * @return Shopping
     */
    public function setSolde($solde)
    {
        $this->solde = $solde;

        return $this;
    }

    /**
     * Get solde
     *
     * @return bool
     */
    public function getSolde()
    {
        return $this->solde;
    }
}
