<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 * @UniqueEntity(fields={"email"})
 * @UniqueEntity(fields={"username"})
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="date_naissance", type="string", length=255, nullable=true)
     */
    protected $date_naissance;

    /**
     * @var string
     *
     * @ORM\Column(name="adresse", type="string", length=255, nullable=true)
     */
    protected $adresse;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_dakar", type="boolean", nullable=true)
     */
    private $is_dakar;

    /**
     * @var boolean
     *
     * @ORM\Column(name="likes", type="boolean", nullable=true)
     */
    private $like;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, nullable=true)
     */
    private $nom;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=255, nullable=true)
     */
    private $prenom;


    /**
     * @var string
     *
     * @ORM\Column(name="photo", type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="telephone", type="string", length=255, nullable=true)
     */
    private $telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=255, nullable=true)
     */
    private $sexe;

    /**
     * @var integer
     *
     * @ORM\Column(name="poids", type="integer", nullable=true)
     */
    private $poids;

    /**
     * @var string
     *
     * @ORM\Column(name="code_generate", type="string", length=255, nullable=true)
     */
    private $code_generate;

    /**
     * Many User have Many Tags.
     * @ORM\ManyToMany(targetEntity="TagDecouverte", inversedBy="users", cascade={"persist", "remove"})
     * @ORM\JoinTable(name="user_tags",
     *      joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=true)},
     *      inverseJoinColumns={@ORM\JoinColumn(name="tag_id", referencedColumnName="id", unique=false, nullable=true)}
     *      )
     */
    private $tags;



    /**
     * @var DateTime
     *
     * @ORM\Column(name="dateinscription", type="datetime", nullable=true)
     */
    private $dateinscription;

    public function __construct()
    {
        parent::__construct();
        // your own logic
        $this->dateinscription = new \DateTime();
        $this->tags = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set dateNaissance
     *
     * @param string $dateNaissance
     *
     * @return User
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->date_naissance = $dateNaissance;

        return $this;
    }

    /**
     * Get dateNaissance
     *
     * @return string
     */
    public function getDateNaissance()
    {
        return $this->date_naissance;
    }

    /**
     * Set isDakar
     *
     * @param boolean $isDakar
     *
     * @return User
     */
    public function setIsDakar($isDakar)
    {
        $this->is_dakar = $isDakar;

        return $this;
    }

    /**
     * Get isDakar
     *
     * @return boolean
     */
    public function getIsDakar()
    {
        return $this->is_dakar;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return User
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set prenom
     *
     * @param string $prenom
     *
     * @return User
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Get prenom
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Set photo
     *
     * @param string $photo
     *
     * @return User
     */
    public function setPhoto($photo)
    {
        $this->photo = $photo;

        return $this;
    }

    /**
     * Get photo
     *
     * @return string
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set telephone
     *
     * @param string $telephone
     *
     * @return User
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }

    /**
     * Get telephone
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set dateinscription
     *
     * @param \DateTime $dateinscription
     *
     * @return User
     */
    public function setDateinscription($dateinscription)
    {
        $this->dateinscription = $dateinscription;

        return $this;
    }

    /**
     * Get dateinscription
     *
     * @return \DateTime
     */
    public function getDateinscription()
    {
        return $this->dateinscription;
    }

    /**
     * Set adresse
     *
     * @param string $adresse
     *
     * @return User
     */
    public function setAdresse($adresse)
    {
        $this->adresse = $adresse;

        return $this;
    }

    /**
     * Get adresse
     *
     * @return string
     */
    public function getAdresse()
    {
        return $this->adresse;
    }

    /**
     * Set sexe
     *
     * @param string $sexe
     *
     * @return User
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    /**
     * Get sexe
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Set codeGenerate
     *
     * @param string $codeGenerate
     *
     * @return User
     */
    public function setCodeGenerate($codeGenerate)
    {
        $this->code_generate = $codeGenerate;

        return $this;
    }

    /**
     * Get codeGenerate
     *
     * @return string
     */
    public function getCodeGenerate()
    {
        return $this->code_generate;
    }

    /**
     * Set poids
     *
     * @param integer $poids
     *
     * @return User
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Get poids
     *
     * @return integer
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Set like
     *
     * @param boolean $like
     *
     * @return User
     */
    public function setLike($like)
    {
        $this->like = $like;

        return $this;
    }

    /**
     * Get like
     *
     * @return boolean
     */
    public function getLike()
    {
        return $this->like;
    }

    /**
     * Add tag
     *
     * @param \AppBundle\Entity\TagDecouverte $tag
     *
     * @return User
     */
    public function addTag(\AppBundle\Entity\TagDecouverte $tag)
    {
        $this->tags[] = $tag;

        return $this;
    }

    /**
     * Remove tag
     *
     * @param \AppBundle\Entity\TagDecouverte $tag
     */
    public function removeTag(\AppBundle\Entity\TagDecouverte $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Get tags
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTagsFormatted()
    {
        $tags = $this->tags ;

        $formatted = [];

        foreach ($tags as $tag) {
            $formatted [] = [
                "id" => $tag->getId(),
                "nom" => $tag->getNom(),
                "articles" => $tag->getArticles(),
                "events" => $tag->getEvents()
            ];
        }

        return $formatted;
    }
}
