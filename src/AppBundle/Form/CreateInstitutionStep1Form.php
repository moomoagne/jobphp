<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 19/07/2018
 * Time: 13:17
 */

namespace AppBundle\Form;


use AppBundle\Entity\Categorie;
use AppBundle\Repository\CategorieRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class CreateInstitutionStep1Form extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options) {
        // $validValues = array(2, 4);
        $builder->add('categorie', EntityType::class, array(
            'placeholder' => '',
            'class' => Categorie::class,
            'query_builder' => function(CategorieRepository $er) {
                return $er->createQueryBuilder('u')
                    ->where('u.is_entity = :v')
                    ->setParameter(':v', true);
            },
        ));
        // récupération de la sous catégorie en choississant d'abord une catégorie
        $builder->add('sousCategorie', HiddenType::class, [
            "required" => false
        ]);
    }


    public function getBlockPrefix() {
        return 'CreateInstitutionStep1';
    }
}