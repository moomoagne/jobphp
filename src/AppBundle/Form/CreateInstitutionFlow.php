<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 19/07/2018
 * Time: 13:06
 */

namespace AppBundle\Form;


use Craue\FormFlowBundle\Form\FormFlow;
use Craue\FormFlowBundle\Form\FormFlowInterface;

class CreateInstitutionFlow extends FormFlow
{

    protected function loadStepsConfig()
    {
        return array(
            array(
                'label' => 'Type de point d\'intérêt',
                'form_type' => 'AppBundle\Form\CreateInstitutionStep1Form'
            ),
            array(
                'label' => 'Infos général du point d\'intérêt choisi',
                'form_type' => 'AppBundle\Form\CreateInstitutionStep2Form',
                /*'skip' => function($estimatedCurrentStepNumber, FormFlowInterface $flow) {
                    return $estimatedCurrentStepNumber > 1 /*&& !$flow->getFormData()->canHaveEngine();
                }*/
            ),
            array(
                'label' => 'confirmation',
            ),
        );
    }

}