<?php

namespace AppBundle\Form;

use AppBundle\Entity\Tag;
use Bnbc\UploadBundle\Form\Type\AjaxfileType;
use Jb\Bundle\FileUploaderBundle\Form\Type\ImageAjaxType;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Form\Type\TagType;

class EventType extends AbstractType
{
    private $container;
    private $request;

    public function __construct(ContainerInterface $container, RequestStack $requestStack)
    {
        $this->container = $container;
        $this->request = $requestStack;
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder
            ->add('dateEvent')
            ->add('description')
            ->add('nom')
            ->add('photo', FileType::class, array(
                'data_class' => null,
                'required' => false
            ))
            ->add('telephone')
            ->add('email')
            ->add('lieu')
            ->add('siteweb')
            ->add('facebook')
            ->add('twitter')
            ->add('instagram')
            ->add('institution')
            ->add('categorie')
        ;

        $builder->add('tags');

    }/**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Event'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_event';
    }


}
