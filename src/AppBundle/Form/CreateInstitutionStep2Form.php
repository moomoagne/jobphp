<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 19/07/2018
 * Time: 13:19
 */

namespace AppBundle\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;

class CreateInstitutionStep2Form extends AbstractType
{
    private $em;
    private $container;

    public function __construct(EntityManagerInterface $entityManager, ContainerInterface $container)
    {
        $this->em = $entityManager;
        $this->container = $container;
    }

    public function buildForm(FormBuilderInterface $builder, array $options) {

        $categories = $this->em->getRepository('AppBundle:Categorie')->findBy(array('is_entity' => true));

        $flow = $this->container->get('appbundle.form.flow.createInstittution');


        $builder
            ->add('nom')
            ->add('adresse')
            ->add('telephone')
            ->add('offre');
            if(in_array($flow->getFormData()->getCategorie(), $categories, true)) {
                if($flow->getFormData()->getCategorie()->getIdentifiant() == "restaurants") {
                    $builder->add('promo', ChoiceType::class, array(
                        'label' => 'Définir si le restaurant est en promo ?',
                        'choices' => array(
                            'Oui' => 1,
                            'No' => 0,
                        ),
                    ));
                }

                if($flow->getFormData()->getCategorie()->getIdentifiant() != "prestataires") {
                    $builder
                        ->add('description')
                        ->add('price', ChoiceType::class, array(
                            'choices'  => array(
                                '$' => '$',
                                '$$' => '$$',
                                '$$$' => '$$$',
                                '$$$$' => '$$$$',
                            ),
                        ))
                        ->add('longitude')
                        ->add('latitude')
                        ->add('facebook')
                        ->add('twitter')
                        ->add('instagram')
                        ->add('photo', FileType::class, array(
                            "required" => false,
                            "data_class" => null
                        ))
                        ->add('etiquettes')
                    ;
                }
            }
    }

    public function getBlockPrefix() {
        return 'createInstitutionStep2';
    }
}